# Feeback File Verification Scripts

Collection of R-scripts for a verification based on feedback files.

## Notes

- the R scripts (scripts folder) are meant to run on my account at DWD and need to be adapted for your system environment.
- The scritps are tested with R 3.5.1.
- The scripts require the non CRAN package Rfdbk (currently on https://gitlab.com/rfxf/Rfdbk).
- The verification results (Rdata files) are meant to be visualized with shiny applications (e.g. http://www.cosmo-model.org/shiny/users/fdbk/)
but can be further edited in R at the users own discretion.


## How To Install
```{bash eval=FALSE}
git clone git@gitlab.com:rfxf/FFV.git
```


## How To Run In Batch Mode

```{bash eval=FALSE}
# SYNOP verification on 6 cores
Rscript YOURPATH/FFV/scripts/fdbk_veri_synop_single.R YOURPATH/FFV/namelists/namelist 6
Rscript YOURPATH/FFV/scripts/fdbk_aggregate_synop_single.R YOURPATH/FFV/namelists/namelist 6
```

## Documentation

A detailed documentation can be found on the COSMO server:
http://www.cosmo-model.org/shiny/users/fdbk/RfdbkVeriDoku.html
