require(Rfdbk)
setDTthreads(1)
options(scipen=3)

# PRINT USED PAKAGES AND VERSION
print(data.table((.packages()),sapply(lapply((.packages()),packageVersion),as.character)))


# LOAD NAMELIST
args         = commandArgs(trailingOnly = TRUE)
namelist     = read.table(args[1])
# namelist    = read.table("/hpc/rhome/routver/ffundel/FFV/namelists/AIREPAGG_EPS_CDEE_ROUTINES.nl")
namelist     = lapply(split(namelist$V2,namelist$V1),as.character)
expID        =  strsplit(namelist[["expIds"]],",")[[1]]
shinyServer  = namelist[["shinyServer"]]
shinyAppPath = namelist[["shinyAppPath"]]
sigTest      = T
if (!is.null(namelist[["sigTest"]])){ if (namelist[["sigTest"]]%in%c("F","FALSE")){sigTest=F}}
print(paste0("TEST FOR SIGNIFICANCE: ",sigTest))
aggMixed     = F
if (!is.null(namelist[["aggMixed"]])){ if (namelist[["aggMixed"]]%in%c("T","TRUE")){aggMixed=T}}
print(str(namelist))

# SET NUMBER OF CORES. IF NOT SET, ONLY ONE CORE WILL BE USED
mc.cores = ifelse(!is.na(args[2]),as.numeric(args[2]),1)
print(paste0("USING ",mc.cores," CORE(S)"))


# SELECT SCORE FILES FOR AGGREGATION
dateslist  = read.table("/hpc/rhome/routver/ffundel/FFV/changes_para_p1")
dateslist  = lapply(split(dateslist$V2,dateslist$V1),as.character)


#RECALCULATE LAST, COMPLETE ROUTINE PERIOD
if (!is.null(namelist[["rerunLatest"]]) & grepl("LAST",namelist[["experiment"]])){
	if (namelist[["rerunLatest"]]%in%c("T","TRUE")){
		if (namelist[["experiment"]]=="LASTP1")    namelist[["stopIniDate"]] = last(dateslist[["start_p1"]])
		if (namelist[["experiment"]]=="LASTPARA")  namelist[["stopIniDate"]] = last(dateslist[["start_para"]])
		if (namelist[["experiment"]]=="LASTCDE")   namelist[["stopIniDate"]] = last(dateslist[["start_cdep"]])
		dateslist = lapply(lapply(lapply(dateslist,sort,decreasing=T),"[",-1),sort) # remove last entry
	}
}

files = dir(namelist[["outDir"]],full.names = T,pattern=paste0(namelist[["expDescr"]],"_",namelist[["filePattern"]]))
if (namelist[["experiment"]]=="LASTP1"){
	files      = files[(grep(last(dateslist[["start_p1"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_p1"]]))
}else if (namelist[["experiment"]]=="LASTPARA"){
	files      = files[(grep(last(dateslist[["start_para"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_para"]]))
}else if (namelist[["experiment"]]=="LASTCDE"){
	files      = files[(grep(last(dateslist[["start_cdep"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_cdep"]]))
}else if (namelist[["experiment"]]=="LASTCDEPS"){
	files      = files[(grep(last(dateslist[["start_cdepsp"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_cdepsp"]]))
}else{
	if (length(c(namelist[["startValDate"]],namelist[["stopValDate"]]))==2){
		filesind   = which(str_sub(files,-10)%between%c(namelist[["startValDate"]],namelist[["stopValDate"]]))
		files      = files[filesind]
	}
}



# LOAD ALL SCORE FILES
CONTSCORES = rbindlist(mclapply(files,function(file){  	
					load(file) 
					mods = unique(ALLSCORES$ALLCONTSCORES$veri_model)
					print(mods)
					if (! any(is.na(match(expID,mods))) | aggMixed==T){
						print(paste0("LOADING ",file))
						return(ALLSCORES$ALLCONTSCORES)
					}else{
						print(paste0("OMITTING ",file))
						return(NULL)
					}},mc.cores=mc.cores,mc.preschedule=F), use.names=TRUE)



# REMOVE UNWANTED RUNS
if (!is.null(namelist$iniTimes)){
	print("REMOVING UNWANTED RUNS")
	CONTSCORES = CONTSCORES[as.numeric(ini_time)%in%as.numeric(unlist(strsplit(namelist$iniTimes,",")))]
}


# DELETE DATES WITH LESS THAN MAXIMUM POSSIBLE MODELS AVAILABLE
if (aggMixed==F){
    print("REMOVING INCOMPLETE DATES I")
    delDates   = CONTSCORES[,uniqueN(veri_model),by=valid_time][V1<length(expID)]$valid_time
    CONTSCORES = CONTSCORES[!valid_time%in%delDates]
}

# DELETE MODELS NOT IN NAMELIST
print("REMOVING INCOMPLETE DATES II")
CONTSCORES = CONTSCORES[veri_model%in%expID]


# TRIM VERIFICATION PERIOD BY INITIALIZATION DATE
print("TRIMMING VERIFICATION PERIOD BY RUNS")
if (namelist[["experiment"]]=="LASTP1"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_p1"]]),format="%Y%m%d%H")]
}
if (namelist[["experiment"]]=="LASTPARA"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_para"]]),format="%Y%m%d%H")]
}
if (namelist[["experiment"]]=="LASTCDE"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_cdep"]]),format="%Y%m%d%H")]
}
if (namelist[["experiment"]]=="LASTCDEPS"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_cdepsp"]]),format="%Y%m%d%H")]
}
if (!is.null(namelist$startIniDate)){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(namelist$startIniDate,format="%Y%m%d%H")]
}
if (!is.null(namelist$stopIniDate)){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600<=as.POSIXct(namelist$stopIniDate,format="%Y%m%d%H")]
}


# RELABEL LEAD-TIME TO TIME OF DAY
if (!is.null(namelist[["expType"]])){
	if (namelist[["expType"]]=="singlerun"){
		print("CONVERTING LEAD-TIME TO TIME OF DAY")
		CONTSCORES[,veri_forecast_time:=(as.numeric(ini_time)+veri_forecast_time)%%24]
	}
}


# RENAME RMSE
print("RENAMING RMSE")
setnames(CONTSCORES,"SKILL(RMSE)","RMSE")


# AGGREGATION 
strat       = c("varno","veri_model","dom","level","veri_forecast_time")
aggr        = c("ini_time","valid_time","valid_hour")
notAggCols  = c(aggr)[which(CONTSCORES[, lapply(.SD, uniqueN), .SDcols=c(aggr)]==1)]
notAggVals  = CONTSCORES[1,notAggCols,with=F]
aggr        = aggr[!aggr%in%notAggCols]
aggrList    = NULL
for (i in 1:length(aggr)){
 aggrList = c(aggrList,lapply(combn(aggr,i,simplif=F),c,strat))
}
aggrList[[length(aggrList)+1]]=strat


print("AGGREGATING CONTINUOUS SCORES")
scorelist <- function(CRPS,IGN,OUTLIER,SPREAD,RMSE,SD,ME,LEN){
	sumLEN        = sum(LEN,na.rm=T)
	MELEN         = ME*LEN
	MSEW          = LEN*RMSE^2
	CRPS          = sum(CRPS*LEN/sumLEN,na.rm=T)
	IGN           = sum(IGN*LEN/sumLEN,na.rm=T)
	OUTLIER       = sum(OUTLIER*LEN/sumLEN,na.rm=T)
	SPREAD        = sum(SPREAD*LEN/sumLEN,na.rm=T)
	"SKILL(RMSE)" = sqrt(sum(MSEW, na.rm = T)/sum(LEN,na.rm = T))
	SD            = sqrt(sum(MSEW, na.rm = T)/sum(LEN, na.rm = T) - (sum(MELEN, na.rm = T)/sum(LEN, na.rm = T))^2)
	ME            = sum(MELEN/sumLEN,na.rm=T)
	LEN           = sumLEN
	return(list(CRPS=CRPS,IGN=IGN,OUTLIER=OUTLIER,SPREAD=SPREAD,"SKILL(RMSE)"=`SKILL(RMSE)`,SD=SD,ME=ME,LEN=LEN))
}
CONTSCORES = groupingsets(CONTSCORES,scorelist(CRPS,IGN,OUTLIER,SPREAD,RMSE,SD,ME,LEN),by=c(strat,aggr),sets=aggrList)
for (col in aggr) eval(parse(text=paste0("CONTSCORES[is.na(",col,"),",col,":='ALL']")))


# ADD NOT AGGREGATED COLUMN
if (any(dim(notAggVals)>0)){
	print("ADDING NOT AGGREGATED VALUES TO SCORE FILES")
	for (i in 1:(dim(notAggVals)[2])) eval(parse(text=paste0("CONTSCORES[,",names(notAggVals)[i],":=notAggVals[1,",i,"]]")))
}

# ADD VERIFICATION PERIOD COLUMN
print("ADDING VERIFICATION PERIOD")
period = paste0(range(unique(CONTSCORES[valid_time!="ALL"]$valid_time)),collapse="-")
CONTSCORES[,veri_period:=period]
# LEADING 0 TO veri_forecast_time TO ENSURE CORRECT SORTING
print("ADDING LEADING 0 TO FORECAST TIME")
levels = sort(unique(CONTSCORES$veri_forecast_time))
labels = str_pad(sort(unique(CONTSCORES$veri_forecast_time)),3,pad=0)
CONTSCORES[,veri_forecast_time:=factor(veri_forecast_time,levels=levels,labels=labels)]
# REORDER DOMAINS
print("REORDERING DOMAINS")
levels = c("ALL","NH","SH","TR","CEU","CDE","GER","EU","NA","SA","SP","AS","AF","AA","FABEC")
levels = union(levels,unique(CONTSCORES$dom))
CONTSCORES[,dom:=factor(dom,levels=levels,labels=levels)]
# REORDER DATA COLUMNS
print("REORDERING DATA")
stratNames = c("varno","veri_model","dom","ini_time","valid_hour","level","veri_forecast_time","valid_time","veri_period")
scoreNames = names(CONTSCORES)[!names(CONTSCORES)%in%stratNames]
setcolorder(CONTSCORES,c(stratNames,scoreNames))


# DELETE TOO HIGH IGN VALUES
print("DELETING IGNORACE SCORES > 15")
CONTSCORES[IGN>15 , IGN:=NA]


# ADD SPREAD SKILL RATIO
print("ADDING SPREAD SKILL RATIO")
CONTSCORES[,"SPREAD/SKILL":=CONTSCORES[["SPREAD"]]/CONTSCORES[["SKILL(RMSE)"]]]
CONTSCORES[,"SPREAD/SD":=CONTSCORES[["SPREAD"]]/CONTSCORES[["SD"]]]


# IN CASE OF ONLY ONE INITIALIZATION TIME REMOVE INI_TIME=="ALL"
print("REMOVING DOUPLICATED SCORES FOR INDIVIDUAL RUNS")
if (length(unique(CONTSCORES[ini_time!="ALL"]$ini_time))==1) CONTSCORES = CONTSCORES[ini_time!="ALL"] 


# ORDER MODEL NAMES ACCORDING TO NAMELIST
print("REORDERING EXPERIMENTS")
CONTSCORES[,veri_model:=factor(veri_model,levels=expID,labels=expID)]


# SIGNIFICANCE TEST FOR CONTINUOUS SCORES AS FUNCTION OF LEAD_TIME
if (sigTest==T & length(unique(CONTSCORES$veri_model))>1){
	# SIGNIFICANCE TEST FOR CONTINUOUS SCORES AS FUNCTION OF LEAD_TIME
	print("RUNNING SIGNIFICANCE TEST FOR CONTINUOUS SCORES")
	setkey(CONTSCORES,"valid_time")
	XX = CONTSCORES[ini_time!="ALL" & valid_time!="ALL" & veri_forecast_time!="ALL" & valid_hour=="ALL"]
	XX[,veri_model:=as.factor(veri_model)]
	lfunc <- function(models){
		model1=models[[1]][1]
		model2=models[[1]][2]
		s = XX[,list(sigCRPS            = as.numeric(tryCatch(ifelse(0%between%t.test(CRPS[veri_model==model1]-CRPS[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    sigIGN             = as.numeric(tryCatch(ifelse(0%between%t.test(IGN[veri_model==model1]-IGN[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    sigOUTLIER         = as.numeric(tryCatch(ifelse(0%between%t.test(OUTLIER[veri_model==model1]-OUTLIER[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
   			    sigSPREAD          = as.numeric(tryCatch(ifelse(0%between%t.test(SPREAD[veri_model==model1]-SPREAD[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),    
			    "sigSKILL(RMSE)"   = as.numeric(tryCatch(ifelse(0%between%t.test(`SKILL(RMSE)`[veri_model==model1]-`SKILL(RMSE)`[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    sigSD              = as.numeric(tryCatch(ifelse(0%between%t.test(SD[veri_model==model1]-SD[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    sigME              = as.numeric(tryCatch(ifelse(0%between%t.test(ME[veri_model==model1]-ME[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    sigLEN             = as.numeric(tryCatch(ifelse(0%between%t.test(LEN[veri_model==model1]-LEN[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    "sigSPREAD/SKILL"  = as.numeric(tryCatch(ifelse(0%between%t.test(`SPREAD/SKILL`[veri_model==model1]-`SPREAD/SKILL`[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    "sigSPREAD/SD"     = as.numeric(tryCatch(ifelse(0%between%t.test(`SPREAD/SD`[veri_model==model1]-`SPREAD/SD`[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
			    mod1    = model1,
			    mod2    = model2),
		       by=c("varno","dom","ini_time","level","veri_forecast_time")]
		return(s)
	}
	sig = do.call(.rbind.data.table,mclapply(apply(combn(levels(XX$veri_model),2),2,list),lfunc,mc.cores=mc.cores,mc.preschedule=F))
	sig[is.na(sigCRPS),sigCRPS:=0]
	sig[is.na(sigIGN),sigIGN:=0]
	sig[is.na(sigOUTLIER),sigOUTLIER:=0]
	sig[is.na(sigSPREAD),sigSPREAD:=0]
	sig[is.na(`sigSKILL(RMSE)`),"sigSKILL(RMSE)":=0]
	sig[is.na(sigSD),sigSD:=0]
	sig[is.na(sigME),sigME:=0]
	sig[is.na(sigSD),sigSD:=0]
	sig[is.na(sigLEN),sigLEN:=0]
	sig[is.na(`sigSPREAD/SKILL`),"sigSPREAD/SKILL":=0]
	sig[is.na(`sigSPREAD/SD`),"sigSPREAD/SD":=0]
	sig[,sigStr:=paste0(sigCRPS,sigIGN,sigOUTLIER,sigSPREAD,`sigSKILL(RMSE)`,sigSD,sigME,sigLEN,`sigSPREAD/SKILL`,`sigSPREAD/SD`)]
	sig[,c("sigCRPS","sigIGN","sigOUTLIER","sigSPREAD","sigSKILL(RMSE)","sigSD","sigME","sigLEN","sigSPREAD/SKILL","sigSPREAD/SD"):=NULL]
	sig[,mod1:=match(sig$mod1,levels(XX$veri_model))]
	sig[,mod2:=match(sig$mod2,levels(XX$veri_model))]
	sig[,sigStr:=paste0(mod1,mod2,"_",sigStr)]
	sig[,c("mod1","mod2"):=NULL]
	sig[,sigStr:=paste0(sigStr,collapse="_"),by=c("varno","dom","ini_time","level","veri_forecast_time")]
	sig = unique(sig)
	sig[,valid_hour:="ALL"]
	rm(XX)
}




# SAVE AND COPY SCOREFILE FOR SHINY APPS
print("SAVING SCORE TIME SERIES")
scores   = CONTSCORES[valid_time!="ALL" & valid_hour=="ALL"]
scores   = scores[,!"valid_hour",with=F]
filename =  paste0(namelist[["outDir"]],"/AIREPe_TS","_",namelist[["fileDescr"]],".Rdata")
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_temp_cont_ts/dataeps/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_temp_cont_ts/dataeps/"))
	system(paste0("rm ",filename))
}

print("SAVING DOMAIN AVERAGE SCORES")
scores   = CONTSCORES[valid_time=="ALL"]
scores[,valid_time:=NULL]
if (sigTest==T  & length(unique(CONTSCORES$veri_model))>1){
		scores   = merge(scores,sig,all.x=T)	# Add significance string
}
filename = paste0(namelist[["outDir"]],"/AIREPe","_",namelist[["fileDescr"]],".Rdata")
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_temp_cont/dataeps/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_temp_cont/dataeps/"))
	system(paste0("rm ",filename))
}

print(warnings())
print(system("qstat -f -J $PBS_JOBID ",intern=T))
print(paste0("R used ",sum(gc()[,6])/1e3," Gb"))



