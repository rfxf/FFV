require(Rfdbk)
require(parallel)
options(scipen=3)
setDTthreads(1)
print(paste0("data.table is working on ",getDTthreads()," thread(s)"))


# PRINT USED PAKAGES AND VERSION
print(data.table((.packages()),sapply(lapply((.packages()),packageVersion),as.character)))

# LOAD NAMELIST
args         = commandArgs(trailingOnly = TRUE)
namelist     = read.table(args[1])
# namelist = read.table("/hpc/rhome/routver/ffundel/FFV/namelists/SYNOPAGG_EPS_ICEUE_CDEE_MNTHLY.nl.dev")
# namelist = read.table("namelist_vepSYNOP.nl")
namelist     = lapply(split(namelist$V2,namelist$V1),as.character)
expID        =  strsplit(namelist[["expIds"]],",")[[1]]
shinyServer  = namelist[["shinyServer"]]
shinyAppPath = namelist[["shinyAppPath"]]
sigTest      = T
if (!is.null(namelist[["sigTest"]])){ if (namelist[["sigTest"]]%in%c("F","FALSE")){sigTest=F}}
print(paste0("TEST FOR SIGNIFICANCE: ",sigTest))
aggMixed     = F
if (!is.null(namelist[["aggMixed"]])){ if (namelist[["aggMixed"]]%in%c("T","TRUE")){aggMixed=T}}
print(str(namelist))

# SET NUMBER OF CORES. IF NOT SET, ONLY ONE CORE WILL BE USED
mc.cores = 1
mc.cores = ifelse(!is.na(args[2]),as.numeric(args[2]),1)
print(paste0("USING ",mc.cores," CORE(S)"))

# SELECT SCORE FILES FOR AGGREGATION
dateFile = "/hpc/rhome/routver/ffundel/FFV/changes_para_p1"
dateslist = NULL
if (file.exists(dateFile)){
	dateslist  = read.table(dateFile)
	dateslist  = lapply(split(dateslist$V2,dateslist$V1),as.character)
}

#RECALCULATE LAST, COMPLETE ROUTINE PERIOD
if (!is.null(namelist[["rerunLatest"]]) & grepl("LAST",namelist[["experiment"]]) & !is.null(dateslist)){
	if (namelist[["rerunLatest"]]%in%c("T","TRUE")){
		if (namelist[["experiment"]]=="LASTP1")    namelist[["stopIniDate"]] = last(dateslist[["start_p1"]])
		if (namelist[["experiment"]]=="LASTPARA")  namelist[["stopIniDate"]] = last(dateslist[["start_para"]])
		if (namelist[["experiment"]]=="LASTCDEPS")   namelist[["stopIniDate"]] = last(dateslist[["start_cdepsp"]])
		dateslist = lapply(lapply(lapply(dateslist,sort,decreasing=T),"[",-1),sort) # remove last entry
	}
}

# INTERMEADIATE SCORE FILES
files      = dir(namelist[["outDir"]],full.names = T,pattern=paste0(namelist[["expDescr"]],"_",namelist[["filePattern"]]))
if (namelist[["experiment"]]=="LASTP1" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_p1"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_p1"]]))
}else if (namelist[["experiment"]]=="LASTPARA" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_para"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_para"]]))
}else if (namelist[["experiment"]]=="LASTCDEPS" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_cdepsp"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_cdepsp"]]))
}else{
	if (length(c(namelist[["startValDate"]],namelist[["stopValDate"]]))==2){
		filesind   = which(str_sub(files,-10)%between%c(namelist[["startValDate"]],namelist[["stopValDate"]]))
		files      = files[filesind]
	}
}

# FUNCTION TO TRIM SCORE FILES FOR UNWANTED SCORES
trimfun <- function(SCORES){
	# TRIM VERIFICATION PERIOD BY INITIALIZATION DATE
	if (namelist[["experiment"]]=="LASTP1"){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")>=as.POSIXct(last(dateslist[["start_p1"]]),format="%Y%m%d%H")]
	}
	if (namelist[["experiment"]]=="LASTPARA"){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")>=as.POSIXct(last(dateslist[["start_para"]]),format="%Y%m%d%H")]
	}
	if (namelist[["experiment"]]=="LASTCEU"){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")>=as.POSIXct(last(dateslist[["start_ceup"]]),format="%Y%m%d%H")]
	}
	if (namelist[["experiment"]]=="LASTCDEPS"){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")>=as.POSIXct(last(dateslist[["start_cdepsp"]]),format="%Y%m%d%H")]
	}
	if (!is.null(namelist$startIniDate)){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")>=as.POSIXct(namelist$startIniDate,format="%Y%m%d%H")]
	}
	if (!is.null(namelist$stopIniDate)){
		SCORES = SCORES[as.POSIXct(SCORES$veri_initial_date,format="%Y%m%d%H%M")<=as.POSIXct(namelist$stopIniDate,format="%Y%m%d%H")]
	}

	# RENAME JJ TO TMAX FOR OLDER VERSION SCORE FILES
	SCORES[varno=="JJ_12h",varno:="TMAX_12h"]

	# REMOVE TMIN & TMAX SCORES AT UNWANTED TIMES
	SCORES[,valid_hour:=format(as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600,"%H")]
	SCORES = SCORES[!(varno=="TMIN_12h" & dom%in%c("N-W Siberia","E Siberia","S-W Siberia","AUS,NZ,INDON") & valid_hour!="00")]
	SCORES = SCORES[!(varno=="TMIN_12h" & dom%in%c("GER","CEU","CDE","China","Kazakhstan","N Africa","S Africa","N Europe","Mediterranean") & valid_hour!="06")]
	SCORES = SCORES[!(varno=="TMIN_12h" & dom%in%c("N Southamerica","S Southamerica") & valid_hour!="12")]
	SCORES = SCORES[!(varno=="TMIN_12h" & dom%in%c("Arctic") & !valid_hour%in%c("06","18"))]
	SCORES = SCORES[!(varno=="TMIN_12h" & dom%in%c("Antarctic") & !valid_hour%in%c("00","12"))]
	SCORES = SCORES[!(varno=="TMAX_12h" & dom%in%c("N-W Siberia","E Siberia","S-W Siberia","AUS,NZ,INDON") & valid_hour!="12")]
	SCORES = SCORES[!(varno=="TMAX_12h" & dom%in%c("GER","CEU","CDE","China","Kazakhstan","N Africa","S Africa","N Europe","Mediterranean") & valid_hour!="18")]
	SCORES = SCORES[!(varno=="TMAX_12h" & dom%in%c("N Southamerica","S Southamerica") & valid_hour!="00")]
	SCORES = SCORES[!(varno=="TMAX_12h" & dom%in%c("Arctic") & !valid_hour%in%c("06","18"))]
	SCORES = SCORES[!(varno=="TMAX_12h" & dom%in%c("Antarctic") & !valid_hour%in%c("00","12"))]
	SCORES[,valid_hour:=NULL]

	# RENAME MODEL IF DATELIST IS GIVEN
	if (!is.null(namelist[["dateList"]])){
		print("RENAMING MODELS ACCORDING TO DATELIST")
		dateList = fread(namelist[["dateList"]],header=F)$V1
		SCORES[substr(format(as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600,"%Y%m%d%H%M"),1,nchar(dateList)[1])%in%dateList,veri_model:=paste0(veri_model,"+")]
		SCORES[!substr(format(as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600,"%Y%m%d%H%M"),1,nchar(dateList)[1])%in%dateList,veri_model:=paste0(veri_model,"-")]
	}



	return(SCORES)
}

# LOAD SCORE FILES PARALLEL
parload <- function(file,type="CONTSCORES"){
		load(file)
		OUT = ALLSCORES[[type]]
		rm(ALLSCORES)
		if(all(expID%in%unique(OUT$veri_model)) | any(grepl(".c",unique(OUT$veri_model))) | aggMixed==T ){
			if (!any(grepl(".c",unique(OUT$veri_model))))	OUT = OUT[veri_model%in%expID]		
			OUT = trimfun(OUT)
			print(paste0("LOADING ",type," FROM ",file))
			return(OUT)	
		}else{
			return(data.table())
		}
}

print("LOADING CONTINUOUS SCORES")
CONTSCORES = rbindlist(mclapply(files,parload,type="CONTSCORES",mc.cores=mc.cores),use.names=TRUE,fill=TRUE)
print("LOADING RELIABILTY SCORES")
RELSCORES  = rbindlist(mclapply(files,parload,type="RELSCORES",mc.cores=mc.cores),use.names=TRUE,fill=TRUE)
print("LOADING CATEGORICAL SCORES")
CATSCORES  = rbindlist(mclapply(files,parload,type="CATSCORES",mc.cores=mc.cores),use.names=TRUE,fill=TRUE)

# CHECK IF ANY SCORE CATEGORY IS EMPTY
doCONT = ifelse(all(dim(CONTSCORES)==0),F,T)
doREL  = ifelse(all(dim(RELSCORES)==0),F,T)
doCAT  = ifelse(all(dim(CATSCORES)==0),F,T)

# STOP IF NO SCORES PRESENT
if (!any(c(doCONT,doCAT,doREL))){ stop("ONLY EMPTY SCORES!") }

# REMOVE DUPLICATED
if (doCONT){ setorder(CONTSCORES,-len); CONTSCORES = CONTSCORES[which(!duplicated(CONTSCORES[,c(1:5,15)]))]  }

# VERIFICATION PERIOD
print("CALCULATING VERIFICATION PERIOD")
if (doCONT){
	veri_period = paste0(format(range(unique(CONTSCORES[,c("veri_initial_date","veri_forecast_time")])[,veri_initial_date:=as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600]$veri_initial_date),"%Y%m%d%H"),collapse="-")
} else {
	veri_period = paste0(format(range(unique(CATSCORES[,c("veri_initial_date","veri_forecast_time")])[,veri_initial_date:=as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600]$veri_initial_date),"%Y%m%d%H"),collapse="-")
}

# ADD INITIAL TIME
print("ADDING INITIAL TIME")
if (doCONT) CONTSCORES[,veri_initial_date:=factor(veri_initial_date)][,ini_time:=factor(veri_initial_date,labels=format(as.POSIXct(levels(CONTSCORES$veri_initial_date),format="%Y%m%d%H%M"),"%H"))]
if (doREL)  RELSCORES[,veri_initial_date:=factor(veri_initial_date)][,ini_time:=factor(veri_initial_date,labels=format(as.POSIXct(levels(RELSCORES$veri_initial_date),format="%Y%m%d%H%M"),"%H"))]
if (doCAT)  CATSCORES[,veri_initial_date:=factor(veri_initial_date)][,ini_time:=factor(veri_initial_date,labels=format(as.POSIXct(levels(CATSCORES$veri_initial_date),format="%Y%m%d%H%M"),"%H"))]

# FUNCTION CREATING A LIST OF ATTRIBUTES TO AGGREGATE ON
aggfun <- function(strat,aggr){
	aggrList = NULL
	for (i in 1:length(aggr)){
	aggrList = c(aggrList,lapply(combn(aggr,i,simplif=F),c,strat))
	}
	aggrList[[length(aggrList)+1]]=strat
	return(aggrList)
}

# AGGREGATION
if (doCONT){
	print("AGGREGATING ENSEMBLE SCORES")
	by = aggfun(c("varno","veri_model","dom"),c("veri_forecast_time","ini_time","z_station","veri_initial_date"))
	AGGCONTSCORES = rbindlist(mclapply(by,function(by) CONTSCORES[,list(CRPS   = sum(crps*len,na.rm=T)/sum(len,na.rm=T),
   		         CRPSF  = sum(crpsf*len,na.rm=T)/sum(len,na.rm=T),
		         SPREAD = sum(spread*len,na.rm=T)/sum(len,na.rm=T),
		         FMEAN  = sum(epsmean*len,na.rm=T)/sum(len,na.rm=T),
		         OMEAN  = sum(obsmean*len,na.rm=T)/sum(len,na.rm=T),
		         ME     = sum((epsmean-obsmean)*len,na.rm=T)/sum(len,na.rm=T),
		         RMSE   = sqrt(sum((rmse^2)*len,na.rm=T)/sum(len,na.rm=T)),
		         SD     = sqrt(sum(len * rmse^2, na.rm = T)/sum(len, na.rm = T) - (sum(len * (epsmean-obsmean), na.rm = T)/sum(len, na.rm = T))^2),
		         LEN    = sum(len,na.rm=T),
			 TALA   = paste0(Reduce("+",lapply(strsplit(talabin,"/"),as.numeric)) ,collapse="/")   ),by=by],mc.cores=mc.cores),use.names=T,fill=T)
	if(length(which(unique(AGGCONTSCORES$ini_time)!="ALL"))==1) AGGCONTSCORES = AGGCONTSCORES[ini_time!="ALL"]
	AGGCONTSCORES[is.na(z_station),z_station:="ALL"][is.na(veri_forecast_time),veri_forecast_time:="ALL"][is.na(ini_time),ini_time:="ALL"][is.na(veri_initial_date),veri_initial_date:="ALL"]
	AGGCONTSCORES[,"spread/skill":=SPREAD/RMSE]
	#AGGCONTSCORES[,"OUTLIER":=100*OUTLIER/LEN]
    AGGCONTSCORES = AGGCONTSCORES[!duplicated(AGGCONTSCORES[,c(1:4,15:17)])]

	#SIGNIFICANCE TEST FOR ENSEMBLE SCORES
    if (sigTest==T & length(unique(CONTSCORES$veri_model))>1){
	    print("RUNNING SIGNIFICANCE TEST FOR CONTINUOUS SCORES")
	    XX = AGGCONTSCORES[ini_time!="ALL" & veri_forecast_time!="ALL" & veri_initial_date!="ALL"]
        setkey(XX,"veri_initial_date")
	    XX[,veri_model:=as.factor(veri_model)]


	    lfunc <- function(models){
		    model1 = models[[1]][1]
		    model2 = models[[1]][2]
		    s = XX[,list(sigCRPS       = as.numeric(tryCatch(ifelse(0%between%t.test(CRPS[veri_model==model1]-CRPS[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            sigCRPSF   = as.numeric(tryCatch(ifelse(0%between%t.test(CRPSF[veri_model==model1]-CRPSF[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            sigSPREAD  = as.numeric(tryCatch(ifelse(0%between%t.test(SPREAD[veri_model==model1]-SPREAD[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            sigRMSE    = as.numeric(tryCatch(ifelse(0%between%t.test(RMSE[veri_model==model1]-RMSE[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            sigSD      = as.numeric(tryCatch(ifelse(0%between%t.test(SD[veri_model==model1]-SD[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            sigME      = as.numeric(tryCatch(ifelse(0%between%t.test(ME[veri_model==model1]-ME[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            #sigOUTLIER = as.numeric(tryCatch(ifelse(0%between%t.test(OUTLIER[veri_model==model1]-OUTLIER[veri_model==model2],conf.level=.95)$conf.int,1,2), error=function(err) 0)),
                            mod1       = model1,
                            mod2       = model2),
                            by = c("varno","dom","ini_time","z_station","veri_forecast_time")]
		    return(s)
	    }
	    sig = do.call(.rbind.data.table,mclapply(apply(combn(levels(XX$veri_model),2),2,list),lfunc,mc.cores=mc.cores,mc.preschedule=F))
	    sig[is.na(sigRMSE),sigRMSE:=0]
	    sig[is.na(sigSD),sigSD:=0]
	    sig[is.na(sigME),sigME:=0]
	    sig[is.na(sigCRPS),sigCRPS:=0]
	    sig[is.na(sigCRPSF),sigCRPSF:=0]
        #sig[is.na(sigOUTLIER),sigOUTLIER:=0]
	    sig[is.na(sigSPREAD),sigSPREAD:=0]
        sig[,sigStr:=paste0(sigCRPS,sigCRPSF,sigSPREAD,sigRMSE,sigSD,sigME)]
	    sig[,c("sigCRPS","sigCRPSF","sigSPREAD","sigRMSE","sigSD","sigME"):=NULL]
	    #sig[,sigStr:=paste0(sigCRPS,sigCRPSF,sigSPREAD,sigRMSE,sigSD,sigME,sigOUTLIER)]
	    #sig[,c("sigCRPS","sigCRPSF","sigSPREAD","sigRMSE","sigSD","sigME","sigOUTLIER"):=NULL]
	    sig[,mod1:=match(sig$mod1,levels(XX$veri_model))]
	    sig[,mod2:=match(sig$mod2,levels(XX$veri_model))]
	    sig[,sigStr:=paste0(mod1,mod2,"_",sigStr)]
	    sig[,c("mod1","mod2"):=NULL]
	    sig[,sigStr:=paste0(sigStr,collapse="_"),by=c("varno","dom","ini_time","z_station","veri_forecast_time")]
	    sig = unique(sig)
        sig[,veri_initial_date:="ALL"]
	    rm(XX)
        AGGCONTSCORES = merge(AGGCONTSCORES,sig,all.x=T)
    }
	AGGCONTSCORES = AGGCONTSCORES[veri_initial_date=="ALL"][,veri_initial_date:=NULL]

	# SAVE TIME SERIES FOR ENSEMBLE SCORES
	scores = CONTSCORES[,talabin:=NULL][,valid_time:=format(as.POSIXct(veri_initial_date,format="%Y%m%d%H")+as.numeric(veri_forecast_time)*3600,"%Y%m%d%H")][,veri_initial_date:=NULL][,veri_period:=veri_period]
	rm(CONTSCORES)
	scores  = .rbind.data.table(scores,scores[,list(z_station  = "ALL",
		         crps    = sum(crps*len,na.rm=T)/sum(len,na.rm=T),
			     crpsf   = sum(crpsf*len,na.rm=T)/sum(len,na.rm=T),
		         spread  = sum(spread*len,na.rm=T)/sum(len,na.rm=T),
		         epsmean = sum(epsmean*len,na.rm=T)/sum(len,na.rm=T),
		         obsmean = sum(obsmean*len,na.rm=T)/sum(len,na.rm=T),
		         rmse    = sqrt(sum((rmse^2)*len,na.rm=T)/sum(len,na.rm=T)),
		         sd      = sqrt(sum(len * rmse^2, na.rm = T)/sum(len, na.rm = T) - (sum(len * (epsmean-obsmean), na.rm = T)/sum(len, na.rm = T))^2),
                 #TODO outlier = sum(outlier,na.rm=T),
		         len     = sum(len,na.rm=T)),by=c("varno","veri_model","dom","ini_time","veri_forecast_time","valid_time","veri_period")][,"spread/skill":=spread/rmse])#[,OUTLIER:=100*outlier/len])

	scores[,ME:=epsmean-obsmean]
	setnames(scores,c("epsmean","obsmean","spread","rmse","sd","crps","crpsf","len","spread/skill"),c("FMEAN","OMEAN","SPREAD","RMSE","SD","CRPS","CRPSF","LEN","SPREAD/SKILL"))
	setcolorder(scores,c("varno","veri_model","dom","ini_time","z_station","veri_forecast_time","valid_time","veri_period","CRPS","CRPSF","SPREAD/SKILL","SPREAD","RMSE","SD","ME","FMEAN","OMEAN","LEN"))
    #setnames(scores,c("epsmean","obsmean","spread","rmse","sd","crps","crpsf","len","spread/skill","outlier"),c("FMEAN","OMEAN","SPREAD","RMSE","SD","CRPS","CRPSF","LEN","SPREAD/SKILL","OUTLIER"))
	#setcolorder(scores,c("varno","veri_model","dom","ini_time","z_station","veri_forecast_time","valid_time","veri_period","CRPS","CRPSF","SPREAD/SKILL","SPREAD","OUTLIER","RMSE","SD","ME","FMEAN","OMEAN","LEN"))
	if (!any(grepl("condition",names(namelist))) & is.null(namelist$dateList)){ scores[,veri_model:=factor(veri_model,levels=expID)] }
    scores = scores[!is.na(dom)]
	print("SAVING SCORES")
	filename = paste0(namelist[["outDir"]],"/EPS","_TS_",namelist[["fileDescr"]],".Rdata")
	save(scores,file=filename)
	rm(scores)
	if (!is.null(shinyServer)){
		system(paste0("chmod uog+r ",filename))
		system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_synop_eps_new_ts/data/"))
		system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_synop_eps_new_ts/data/"))
		system(paste0("rm ",filename))
	}

	#setcolorder(AGGCONTSCORES,unique(unlist(by)))
}

if (doREL){
	print("AGGREGATING RELIABILITY DIAGRAM")
	by = aggfun(c("varno","veri_model","dom","pfcst","thr"),c("veri_forecast_time","ini_time","z_station"))
	AGGRELSCORES = rbindlist(mclapply(by,function(by) RELSCORES[,list(pobs=sum(pobs*len,na.rm=T)/sum(len,na.rm=T),len=sum(len,na.rm=T)),by=by],mc.cores=mc.cores),use.names=T,fill=T)
	AGGRELSCORES[is.na(z_station),z_station:="ALL"][is.na(veri_forecast_time),veri_forecast_time:="ALL"][is.na(ini_time),ini_time:="ALL"]
	if(length(which(unique(AGGRELSCORES$ini_time)!="ALL"))==1) AGGRELSCORES = AGGRELSCORES[ini_time!="ALL"]
	rm(RELSCORES)
	setcolorder(AGGRELSCORES,unique(unlist(by)))
	by  = c("veri_forecast_time","varno","veri_model","dom","thr","ini_time","z_station")
	BS  = AGGRELSCORES[,list(REL=(sum(len*(pfcst-pobs)^2))/sum(len),RES=(sum(len*(pobs-(sum(pobs*len,na.rm=T)/sum(len,na.rm=T)))^2))/sum(len),UNC=(sum(pobs*len,na.rm=T)/sum(len,na.rm=T))*(1-(sum(pobs*len,na.rm=T)/sum(len,na.rm=T)))),by=by]
	BS[,BS:=REL-RES+UNC]
	BS[,BSS:=1-BS/UNC]
	setcolorder(BS,unique(unlist(by)))
}

if (doCAT){
	print("AGGREGATING CONTINGENCY SCORES")
	by = aggfun(c("varno","veri_model","dom","pfcst","thr"),c("veri_forecast_time","ini_time","z_station"))
	AGGCATSCORES = rbindlist(mclapply(by,function(by) CATSCORES[,list(hits=sum(hits,na.rm=T),
		                       misses  = sum(misses,na.rm=T),
		                       false   = sum(false,na.rm=T),
		                       corrneg = sum(corrneg,na.rm=T)),by=by],mc.cores=mc.cores),use.names=T,fill=T)
	AGGCATSCORES[is.na(z_station),z_station:="ALL"][is.na(veri_forecast_time),veri_forecast_time:="ALL"][is.na(ini_time),ini_time:="ALL"]
	if(length(which(unique(AGGCATSCORES$ini_time)!="ALL"))==1) AGGCATSCORES = AGGCATSCORES[ini_time!="ALL"]
	AGGCATSCORES[,POD:=hits/(hits+misses)]
	AGGCATSCORES[,FAR:=false/(corrneg+false)]
	AGGCATSCORES[,pclim:=(hits+misses)/(hits+misses+false+corrneg)]
	rm(CATSCORES)
	setcolorder(AGGCATSCORES,unique(unlist(by)))
}

# ADDING VERIFICATION PERIOD
print("ADDING VERIFICATION PERIOD")
if (doCONT){ setcolorder(AGGCONTSCORES[,veri_period:=veri_period],"veri_period")}
if (doREL){  setcolorder(AGGRELSCORES[,veri_period:=veri_period],"veri_period");setcolorder(BS[,veri_period:=veri_period],"veri_period")}
if (doCAT){  setcolorder(AGGCATSCORES[,veri_period:=veri_period],"veri_period")}

# EXTRACTING TALAGRAND TABLE
if (doCONT){
	print("EXTRACTING TALAGRAND INDEX")
	TALA = data.table()
	for (model in unique(AGGCONTSCORES$veri_model)){
		TALA = .rbind.data.table(TALA,melt(AGGCONTSCORES[veri_model==model,as.list(as.numeric(unlist(strsplit(TALA,"/")))),by=c("veri_model","veri_forecast_time","varno","ini_time","z_station","dom","veri_period")],id.vars=1:7,variable.name="TALABIN",value.name="N"),fill=T)
	}
	TALA[,TALABIN:=as.integer(gsub("V","",TALABIN))]
	AGGCONTSCORES[,TALA:=NULL]
}

# PREPARE FILE TO SAVE
print("PREPARING SCORE FILE")
ALLSCORES = list()
if (doCONT){ ALLSCORES[["CONTSCORES"]] = AGGCONTSCORES;  rm(AGGCONTSCORES)}
if (doREL){  ALLSCORES[["RELD"]]       = AGGRELSCORES;   rm(AGGRELSCORES)}
if (doREL){  ALLSCORES[["BRIER"]]      = BS;             rm(BS)}
if (doCAT){  ALLSCORES[["ROCA"]]       = AGGCATSCORES;   rm(AGGCATSCORES)}
if (doCONT){ ALLSCORES[["TALA"]]       = TALA;           rm(TALA)}


# REMOVE NA DOMAIN
print("REMOVE NA DOMAIN")
if (doCONT){ ALLSCORES[["CONTSCORES"]] = ALLSCORES[["CONTSCORES"]][!is.na(dom)]}
if (doREL){  ALLSCORES[["RELD"]]       = ALLSCORES[["RELD"]][!is.na(dom)]}
if (doREL){  ALLSCORES[["BRIER"]]      = ALLSCORES[["BRIER"]][!is.na(dom)]}
if (doCAT){  ALLSCORES[["ROCA"]]       = ALLSCORES[["ROCA"]][!is.na(dom)]}
if (doCONT){ ALLSCORES[["TALA"]]       = ALLSCORES[["TALA"]][!is.na(dom)]}



# REORDER DOMAINS
print("REORDERING DOMAINS")
levels = c("ALL","NH","SH","TR","CEU","CDE","GER","EU","N Europe","Mediterranean","NA","SA","SP","AS","AF","AA",
		"Arctic","N Northamerica","W Northamerica","E Northamerica","N-W Siberia","E Siberia","S-W Siberia","China","Kazakhstan",
		"AUS,NZ,INDON","N Southamerica","S Southamerica","N Africa","S Africa","Antarctic")
levels = union(levels,unique(ALLSCORES[["CONTSCORES"]]$dom))
if (doCONT){  ALLSCORES[["CONTSCORES"]][,dom:=factor(dom,levels=levels,labels=levels)]}
if (doREL){   ALLSCORES[["RELD"]][,dom:=factor(dom,levels=levels,labels=levels)]}
if (doREL){   ALLSCORES[["BRIER"]][,dom:=factor(dom,levels=levels,labels=levels)]}
if (doCAT){   ALLSCORES[["ROCA"]][,dom:=factor(dom,levels=levels,labels=levels)]}
if (doCONT){  ALLSCORES[["TALA"]][,dom:=factor(dom,levels=levels,labels=levels)]}


# SORT MODELS ACCORDING TO NAMELIST
print("REORDERING MODEL NAMES")
if (!any(grepl("condition",names(namelist))) & is.null(namelist$dateList)){
    if (doCONT){ ALLSCORES[["CONTSCORES"]][,veri_model:=factor(veri_model,levels=expID)]}
    if (doREL){  ALLSCORES[["RELD"]][,veri_model:=factor(veri_model,levels=expID)]}
    if (doREL){  ALLSCORES[["BRIER"]][,veri_model:=factor(veri_model,levels=expID)]}
    if (doCAT){  ALLSCORES[["ROCA"]][,veri_model:=factor(veri_model,levels=expID)]}
    if (doCONT){ ALLSCORES[["TALA"]][,veri_model:=factor(veri_model,levels=expID)]}
}


# SAVE LEAD-TIME SCOREFILE FOR SHINY APPS
print("SAVING SCORES")
filename = paste0(namelist[["outDir"]],"/EPS","_",namelist[["fileDescr"]],".Rdata")
save(ALLSCORES,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_synop_eps_new/data/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_synop_eps_new/data/"))
	system(paste0("rm ",filename))
}

# JOB STATISTICS
print(warnings())
print(system("qstat -f -J $PBS_JOBID ",intern=T))
print(paste0("R used ",sum(gc()[,6])/1e3," Gb"))
