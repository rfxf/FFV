require(Rfdbk)
options(scipen=3)
setDTthreads(1)

# PRINT USED PAKAGES AND VERSION
print(data.table((.packages()),sapply(lapply((.packages()),packageVersion),as.character)))


# LOAD NAMELIST
args         = commandArgs(trailingOnly = TRUE)
namelist     = read.table(args[1])
# namelist    = read.table("/hpc/rhome/routver/ffundel/FFV/namelists/RAD_ICON_TEST.nl")
namelist     = lapply(split(namelist$V2,namelist$V1),as.character)
expID        =  strsplit(namelist[["expIds"]],",")[[1]]
shinyServer  = namelist[["shinyServer"]]
shinyAppPath = namelist[["shinyAppPath"]]
inclEnsMean  = F; if (!is.null(namelist[["inclEnsMean"]])){ if (namelist[["inclEnsMean"]]%in%c("T","TRUE")){inclEnsMean=T}}
print(str(namelist))

# SET NUMBER OF CORES, IF NOT SET, ONLY ONE CORE WILL BE USED
mc.cores = ifelse(!is.na(args[2]),as.numeric(args[2]),1)
print(paste0("USING ",mc.cores," CORE(S)"))


# SELECT SCORE FILES FOR AGGREGATION
dateslist  = read.table("/hpc/rhome/routver/ffundel/FFV/changes_para_p1")
dateslist  = lapply(split(dateslist$V2,dateslist$V1),as.character)
files      = dir(namelist[["outDir"]],full.names = T,pattern=namelist[["filePattern"]])
if (namelist[["experiment"]]=="LASTP1"){
	files      = files[(grep(last(dateslist[["start_p1"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_p1"]]))
}else if (namelist[["experiment"]]=="LASTPARA"){
	files      = files[(grep(last(dateslist[["start_para"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_para"]]))
}else{
	files      = dir(namelist[["outDir"]],full.names = T,pattern=namelist[["filePattern"]])
	if (length(c(namelist[["startValDate"]],namelist[["stopValDate"]]))==2){
		filesind   = which(str_sub(files,-10)%between%c(namelist[["startValDate"]],namelist[["stopValDate"]]))
		files      = files[filesind]
	}
}



# FUNCTION TO AGGREGATE SCORES
scorelist <- function(ME,RMSE,MAE,LEN,OMEAN,FMEAN,R2){
			sumLEN = sum(LEN,na.rm=T)
			MELEN  = ME*LEN
			MSEW   = LEN*RMSE^2
			ME     = sum(MELEN/sumLEN,na.rm=T)
			MAE    = sum((MAE*LEN)/sumLEN,na.rm=T)
			RMSE   = sqrt(sum(MSEW, na.rm = T)/sum(LEN,na.rm = T))
			SD     = sqrt(sum(MSEW, na.rm = T)/sum(LEN, na.rm = T) - (sum(MELEN, na.rm = T)/sum(LEN, na.rm = T))^2)
			R2     = sum((R2*LEN)/sumLEN,na.rm=T)
			OMEAN  = sum((OMEAN*LEN)/sumLEN,na.rm=T)
			FMEAN  = sum((FMEAN*LEN)/sumLEN,na.rm=T)
			LEN    = sumLEN
			return(list(ME=ME,MAE=MAE,RMSE=RMSE,SD=SD,R2=R2,LEN=LEN,OMEAN=OMEAN,FMEAN=FMEAN))
}
	

# FUNCTION TO TRIM DATA
trimfun <- function(DT){
	# REMOVE UNWANTED RUNS
	if (!is.null(namelist$iniTimes)){
		print("REMOVING UNWANTED RUNS")
		DT = DT[as.numeric(ini_time)%in%as.numeric(unlist(strsplit(namelist$iniTimes,",")))]
	}

	# DELETE DATES WITH LESS THAN MAXIMUM POSSIBLE MODELS AVAILABLE
	print("DELETING INCOMPLETE DATES I")
	delDates   = DT[,uniqueN(veri_model),by=valid_time][V1<length(expID)]$valid_time
	DT = DT[!valid_time%in%delDates]

	# DELETE MODELS NOT IN NAMELIST
	print("DELETING INCOMPLETE DATES II")
    if (inclEnsMean) expID = c(expID,paste0(expID,"e"))
	DT = DT[veri_model%in%expID]

	# TRIM VERIFICATION PERIOD BY INITIALIZATION DATE
	print("TRIMMING VERIFICATION RUNS")
	if (namelist[["experiment"]]=="LASTP1" & !is.null(dateslist)){
		DT = DT[as.POSIXct(DT$valid_time,format="%Y%m%d%H%M")-DT$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_p1"]]),format="%Y%m%d%H")]
	}
	if (namelist[["experiment"]]=="LASTPARA" & !is.null(dateslist)){
		DT = DT[as.POSIXct(DT$valid_time,format="%Y%m%d%H%M")-DT$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_para"]]),format="%Y%m%d%H")]
	}
	if (!is.null(namelist$startIniDate)){
		DT = DT[as.POSIXct(DT$valid_time,format="%Y%m%d%H%M")-DT$veri_forecast_time*3600>=as.POSIXct(namelist$startIniDate,format="%Y%m%d%H")]
	}
	if (!is.null(namelist$stopIniDate)){
		DT = DT[as.POSIXct(DT$valid_time,format="%Y%m%d%H%M")-DT$veri_forecast_time*3600<=as.POSIXct(namelist$stopIniDate,format="%Y%m%d%H")]
	}

	# RELABEL LEAD-TIME TO TIME OF DAY
	if (!is.null(namelist[["expType"]])){
		if (namelist[["expType"]]=="singlerun"){
			print("CONVERTING LEAD-TIME TO TIME OF DAY")
			DT[,veri_forecast_time:=(as.numeric(ini_time)+veri_forecast_time)%%24]
		}
	}
	
	return(DT)
} 



# FUNCTION AGGREGATION SCORE FILES IN PARALLEL
parfun <- function(interm){
	# PRESET AGGREGATED SCORES
	AGGSCORES = NULL

	if (is.data.table(interm[[1]])) interm = list(rbindlist(interm, use.names=TRUE))
	
	for (i_f in 1:length(interm)){

		if (!is.data.table(interm[[1]])){
			# LOAD SINGLE SCORE FILES
			file = interm[i_f]
			load(file) 
			mods = unique(ALLSCORES$ALLCONTSCORES$veri_model)
			print(mods)
			if (! any(is.na(match(expID,mods)))){
				print(paste0("LOADING ",file))
				CONTSCORES = ALLSCORES$ALLCONTSCORES
			}else{
				print(paste0("OMITTING ",file))
				next
			}
		}else{
			CONTSCORES = interm[[i_f]]
		}
	
		if (!is.data.table(interm[[1]])){
			CONTSCORES = trimfun(CONTSCORES)
			if ( "valid_time"%in%names(CONTSCORES) ) CONTSCORES = .rbind.data.table(CONTSCORES[,-"valid_time"],AGGSCORES)
		}

		# AGGREGATION 
		strat       = c("varno","veri_model","dom","level","veri_forecast_time","ini_time","valid_hour","level_sig")
		AGGSCORES = CONTSCORES[,scorelist(ME,RMSE,MAE,LEN,OMEAN,FMEAN,R2),by=strat]
		rm(CONTSCORES)

	}
	return(AGGSCORES)
}


# INCREMENTAL, PARALLEL AGGREGATION
groups    = lapply(splitIndices(length(files),min(mc.cores,max(1,floor(length(files)/2)))),function(i) files[i])
AGGSCORES = mclapply(groups,parfun,mc.cores=mc.cores)
AGGSCORES = AGGSCORES[sapply(AGGSCORES,is.data.table)]

while(length(AGGSCORES)>1){
	groups = splitIndices(length(AGGSCORES),min(mc.cores,floor(length(AGGSCORES)/2)))

	AGGSCORES     = mclapply(groups,function(i) parfun(AGGSCORES[i]),mc.cores=mc.cores)
	AGGSCORES     = AGGSCORES[sapply(AGGSCORES,is.data.table)]
}
AGGSCORES = AGGSCORES[[1]]
AGGSCORES = .rbind.data.table(AGGSCORES,AGGSCORES[,scorelist(ME,RMSE,MAE,LEN,OMEAN,FMEAN,R2),by=c("varno","veri_model","dom","level","level_sig","veri_forecast_time","valid_hour")][,ini_time:="ALL"])
if (any(unique(AGGSCORES$valid_hour)!="ALL")){
	AGGSCORES = .rbind.data.table(AGGSCORES,AGGSCORES[,scorelist(ME,RMSE,MAE,LEN,OMEAN,FMEAN,R2),by=c("varno","veri_model","dom","level","level_sig","veri_forecast_time","ini_time")][,valid_hour:="ALL"])
}



# ADD VERIFICATION PERIOD COLUMN
print("ADDING VERIFICATION PERIOD")
veri_period = paste0(range(unlist(mclapply(files,function(i) {load(i);return(as.character(ALLSCORES$ALLCONTSCORES$valid_time[1]))},mc.cores=mc.cores))),collapse="-")
AGGSCORES[,veri_period:=veri_period]

# RENAME VARNO
print("RENAMING VARNO")
AGGSCORES[,varno:=varno_to_name(varno,F)]
# LEADING 0 TO veri_forecast_time TO ENSURE CORRECT SORTING
AGGSCORES[,veri_forecast_time:=str_pad(veri_forecast_time,3,pad=0)]
# REORDER DOMAINS
levels=c("ALL","90S-60S","60S-20S","20S-20N","20N-60N","60N-90N")
AGGSCORES[,dom:=factor(dom,levels=levels,labels=levels)]
# REORDER DATA COLUMNS
stratNames = c("varno","veri_model","dom","ini_time","valid_hour","level","level_sig","veri_forecast_time","veri_period")
scoreNames = names(AGGSCORES)[!names(AGGSCORES)%in%stratNames]
setcolorder(AGGSCORES,c(stratNames,scoreNames))


# IN CASE OF ONLY ONE INITIALIZATION TIME REMOVE INI_TIME=="ALL"
if (length(unique(AGGSCORES[ini_time!="ALL"]$ini_time))==1) AGGSCORES = CONTSCORES[ini_time!="ALL"] 


# ORDER MODEL NAMES ACCORDING TO NAMELIST
AGGSCORES[,veri_model:=factor(veri_model,levels=expID,labels=expID)]


# SAVE AND COPY SCOREFILE FOR SHINY APPS
filename = paste0(namelist[["outDir"]],"/RAD","_",namelist[["fileDescr"]],".Rdata")
scores   = AGGSCORES
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_temp_cont/data/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_temp_cont/data/",basename(filename)))
	system(paste0("rm ",filename))
}


print(warnings())
print(system("qstat -f -J $PBS_JOBID ",intern=T))
print(paste0("R used ",sum(gc()[,6])/1e3," Gb"))



