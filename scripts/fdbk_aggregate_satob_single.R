require(Rfdbk)
options(scipen=3)
setDTthreads(1)

# PRINT USED PAKAGES AND VERSION
print(data.table((.packages()),sapply(lapply((.packages()),packageVersion),as.character)))


# LOAD NAMELIST
args         = commandArgs(trailingOnly = TRUE)
namelist     = read.table(args[1])
#namelist     = read.table("/hpc/rhome/routver/ffundel/FFV/namelists/SATOBAGG_ICON_AND_PARALLELS.nl.tmp")
namelist     = lapply(split(namelist$V2,namelist$V1),as.character)
expID        =  strsplit(namelist[["expIds"]],",")[[1]]
shinyServer  = namelist[["shinyServer"]]
shinyAppPath = namelist[["shinyAppPath"]]
inclEnsMean  = F; if (!is.null(namelist[["inclEnsMean"]])){ if (namelist[["inclEnsMean"]]%in%c("T","TRUE")){inclEnsMean=T}}
print(str(namelist))

# SET NUMBER OF CORES. IF NOT SET, ONLY ONE CORE WILL BE USED
mc.cores = ifelse(!is.na(args[2]),as.numeric(args[2]),1)
print(paste0("USING ",mc.cores," CORE(S)"))


# SELECT SCORE FILES FOR AGGREGATION
dateFile = "/hpc/rhome/routver/ffundel/FFV/changes_para_p1"
dateslist = NULL
if (file.exists(dateFile)){
	dateslist  = read.table(dateFile)
	dateslist  = lapply(split(dateslist$V2,dateslist$V1),as.character)
}

#RECALCULATE LAST, COMPLETE ROUTINE PERIOD
if (!is.null(namelist[["rerunLatest"]]) & grepl("LAST",namelist[["experiment"]]) & !is.null(dateslist)){
	if (namelist[["rerunLatest"]]%in%c("T","TRUE")){
		if (namelist[["experiment"]]=="LASTP1")    namelist[["stopIniDate"]] = last(dateslist[["start_p1"]])
		if (namelist[["experiment"]]=="LASTPARA")  namelist[["stopIniDate"]] = last(dateslist[["start_para"]])
		if (namelist[["experiment"]]=="LASTCDE")   namelist[["stopIniDate"]] = last(dateslist[["start_cdep"]])
		dateslist = lapply(lapply(lapply(dateslist,sort,decreasing=T),"[",-1),sort) # remove last entry
	}
}


files      = dir(namelist[["outDir"]],full.names = T,pattern=paste0(namelist[["expDescr"]],"_",namelist[["filePattern"]]))
if (namelist[["experiment"]]=="LASTP1" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_p1"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_p1"]]))
}else if (namelist[["experiment"]]=="LASTPARA" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_para"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_para"]]))
}else if (namelist[["experiment"]]=="LASTCDE" & !is.null(dateslist)){
	files      = files[(grep(last(dateslist[["start_cdep"]]),files)):length(files)]
	namelist[["fileDescr"]] = paste0(namelist[["fileDescr"]],"_",last(dateslist[["start_cdep"]]))
}else{
	if (length(c(namelist[["startValDate"]],namelist[["stopValDate"]]))==2){
		filesind   = which(str_sub(files,-10)%between%c(namelist[["startValDate"]],namelist[["stopValDate"]]))
		files      = files[filesind]
	}
}


# LOAD ALL SCORE FILES
CONTSCORES = rbindlist(mclapply(files,function(file){  	
					load(file) 
					mods = unique(ALLSCORES$ALLCONTSCORES$veri_model)
					print(mods)
					if (! any(is.na(match(expID,mods)))){
						print(paste0("LOADING ",file))
						return(ALLSCORES$ALLCONTSCORES)
					}else{
						print(paste0("OMITTING ",file))
						return(NULL)
					}},mc.cores=mc.cores,mc.preschedule=F), use.names=TRUE)


# REMOVE UNWANTED RUNS
if (!is.null(namelist$iniTimes)){
	CONTSCORES = CONTSCORES[as.numeric(ini_time)%in%as.numeric(unlist(strsplit(namelist$iniTimes,",")))]
}


# DELETE MODEL NOT MENTIONED IN NAMELIST
if (inclEnsMean) expID = c(expID,paste0(expID,"e"))
CONTSCORES = CONTSCORES[veri_model%in%expID]


# DELETE DATES WITH LESS THAN MAXIMUM POSSIBLE MODELS AVAILABLE
nModels    = max(CONTSCORES[,uniqueN(veri_model),by=valid_time]$V1)
delDates   = CONTSCORES[,uniqueN(veri_model),by=valid_time][V1!=nModels]$valid_time
CONTSCORES = CONTSCORES[!valid_time%in%delDates]


# TRIM VERIFICATION PERIOD BY INITIALIZATION DATE
if (namelist[["experiment"]]=="LASTP1"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_p1"]]),format="%Y%m%d%H")]
}
if (namelist[["experiment"]]=="LASTPARA"){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(last(dateslist[["start_para"]]),format="%Y%m%d%H")]
}
if (!is.null(namelist$startIniDate)){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600>=as.POSIXct(namelist$startIniDate,format="%Y%m%d%H")]
}
if (!is.null(namelist$stopIniDate)){
	CONTSCORES = CONTSCORES[as.POSIXct(CONTSCORES$valid_time,format="%Y%m%d%H%M")-CONTSCORES$veri_forecast_time*3600<=as.POSIXct(namelist$stopIniDate,format="%Y%m%d%H")]
}


# AGGREGATION 
strat     = c("varno","veri_model","dom","level","veri_forecast_time")
aggr      = c("ini_time","valid_time","valid_hour")
notAggCols  = c(aggr)[which(CONTSCORES[, lapply(.SD, uniqueN), .SDcols=c(aggr)]==1)]
notAggVals =  CONTSCORES[1,notAggCols,with=F]
aggr        = aggr[!aggr%in%notAggCols]
aggrList = NULL
for (i in 1:length(aggr)){
 aggrList = c(aggrList,lapply(combn(aggr,i,simplif=F),c,strat))
}
aggrList[[length(aggrList)+1]]=strat

print("AGGREGATING CONTINUOUS SCORES")
scorelist <- function(ME,RMSE,MAE,LEN,R2){
	sumLEN = sum(LEN,na.rm=T)
	MELEN  = ME*LEN
	MSEW   = LEN*RMSE^2
	ME     = sum(MELEN/sumLEN,na.rm=T)
	MAE    = sum((MAE*LEN)/sumLEN,na.rm=T)
	RMSE   = sqrt(sum(MSEW, na.rm = T)/sum(LEN,na.rm = T))
	SD     = sqrt(sum(MSEW, na.rm = T)/sum(LEN, na.rm = T) - (sum(MELEN, na.rm = T)/sum(LEN, na.rm = T))^2)
	R2     = sum((R2*LEN)/sumLEN,na.rm=T)
	LEN    = sumLEN
	return(list(ME=ME,MAE=MAE,RMSE=RMSE,SD=SD,R2=R2,LEN=LEN))
}
CONTSCORES <- groupingsets(CONTSCORES,scorelist(ME,RMSE,MAE,LEN,R2),by=c(strat,aggr),sets=aggrList)
for (col in aggr) eval(parse(text=paste0("CONTSCORES[is.na(",col,"),",col,":='ALL']")))

# ADD NOT AGGREGATED COLUMN
if (any(dim(notAggVals)>0)){
	print("ADDING NOT AGGREGATED VALUES TO SCORE FILES")
	for (i in 1:(dim(notAggVals)[2]))eval(parse(text=paste0("CONTSCORES[,",names(notAggVals)[i],":=notAggVals[1,",i,"]]")))
}
# ADD VERIFICATION PERIOD COLUMN
CONTSCORES[,veri_period:= paste0(range(unique(CONTSCORES[valid_time!="ALL"]$valid_time)),collapse="-")]
# RENAME VARNO
CONTSCORES[,varno:=varno_to_name(varno,F)]
# LEADING 0 TO veri_forecast_time TO ENSURE CORRECT SORTING
CONTSCORES[,veri_forecast_time:=str_pad(veri_forecast_time,3,pad=0)]
# REORDER DOMAINS
levels=c("ALL","90S-60S","60S-20S","20S-20N","20N-60N","60N-90N")
CONTSCORES[,dom:=factor(dom,levels=levels,labels=levels)]
# REORDER DATA COLUMNS
stratNames = c("varno","veri_model","dom","ini_time","valid_hour","level","veri_forecast_time","valid_time","veri_period")
scoreNames = names(CONTSCORES)[!names(CONTSCORES)%in%stratNames]
setcolorder(CONTSCORES,c(stratNames,scoreNames))


# IN CASE OF ONLY ONE INITIALIZATION TIME REMOVE INI_TIME=="ALL"
if (length(unique(CONTSCORES[ini_time!="ALL"]$ini_time))==1) CONTSCORES = CONTSCORES[ini_time!="ALL"] 


# ORDER MODEL NAMES ACCORDING TO NAMELIST
CONTSCORES[,veri_model:=factor(veri_model,levels=expID,labels=expID)]


# SAVE AND COPY SCOREFILE FOR SHINY APPS
scores   = CONTSCORES[valid_time!="ALL"]
filename =  paste0(namelist[["outDir"]],"/SATOB_TS","_",namelist[["fileDescr"]],".Rdata")
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_temp_cont_ts/data/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_temp_cont_ts/data/",basename(filename)))
	system(paste0("rm ",filename))
}

scores   = CONTSCORES[valid_time=="ALL"]
scores[,valid_time:=NULL]
filename = paste0(namelist[["outDir"]],"/SATOB","_",namelist[["fileDescr"]],".Rdata")
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_temp_cont/data/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_temp_cont/data/",basename(filename)))
	system(paste0("rm ",filename))
}

print(warnings())
print(system("qstat -f -J $PBS_JOBID ",intern=T))
print(paste0("R used ",sum(gc()[,6])/1e3," Gb"))



