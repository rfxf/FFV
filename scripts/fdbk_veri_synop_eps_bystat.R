require(Rfdbk)
options(scipen=3)
require(sp)
require(parallel)
require(stringi)
require(SpecsVerification)
setDTthreads(1)

# PRINT USED PAKAGES AND VERSION
print(data.table((.packages()),sapply(lapply((.packages()),packageVersion),as.character)))


# LOAD NAMELIST
args     = commandArgs(trailingOnly = TRUE)
namelist = read.table(args[1])
# namelist = read.table("namelist_vepSYNOP.nl")
namelist        = lapply(split(namelist$V2,namelist$V1),as.character)
expID           =  strsplit(namelist[["expIds"]],",")[[1]]
fdbkDir         =  strsplit(namelist[["fdbkDirs"]],",")[[1]]
shinyServer     = namelist[["shinyServer"]]
shinyAppPath    = namelist[["shinyAppPath"]]
veri_ens_member = if(!is.null(namelist[["veri_ens_member"]])){rep(unlist(strsplit(namelist[["veri_ens_member"]],",")),length(expID))[1:length(expID)]}
mimicVersus     = F; if(!is.null(namelist[["mimicVersus"]])){ mimicVersus = ifelse(namelist[["mimicVersus"]]%in%c("T","TRUE"),T,F)}
whitelists      = if(!is.null(namelist[["whitelists"]])){unlist(strsplit(namelist[["whitelists"]],","))}
blacklists      = if(!is.null(namelist[["blacklists"]])){unlist(strsplit(namelist[["blacklists"]],","))}
nMembers        = as.numeric(strsplit(namelist[["nMembers"]],",")[[1]])
varnos         = unique(varno_to_name(unlist(strsplit(gsub("\\_?..\\h", "", paste0(namelist$varno,collapse=",")),",")),rev=T))
useObsBias      = F; if(!is.null(namelist[["useObsBias"]])){ useObsBias = ifelse(namelist[["useObsBias"]]%in%c("T","TRUE"),T,F)}
domains         = "";if (!is.null(namelist[["subdomains"]])){ domains = unlist(strsplit(namelist[["subdomains"]],","))}
print(str(namelist))


# CHECK VALIDATION STEPS
timeSteps  = as.numeric(strsplit(namelist$timeSteps,",")[[1]])
timeBreaks = as.numeric(strsplit(namelist$timeBreaks,",")[[1]])
if (length(timeSteps)!=(length(timeBreaks)-1)){ print("ERROR: CHECK timeSteps & timeBreaks");stop()}


# CHECK FOR USER DEFINE SUBDOMAINS (POLYGONS OR STATIDS)
if ("USER"%in%domains){
	print("USING USER DEFINED SUBDOMAIN(S)")
	domains  = domains[domains!="SYNOP"]
	domtable = fread(namelist[["domainTable"]],header=T)
	if (all(c("name","lon","lat")%in%names(domtable))){
		print("USING POLYGON DOMAIN TABLE")
		polys    = SpatialPolygons( lapply(unique(domtable$name), function(i) {Polygons(list(Polygon(na.omit(domtable[name==i, c("lon","lat")]))), ID=i)}))
		if (length(polys)>1){		
			comb     = combn(length(polys),2)
			if(any(sapply(1:ncol(comb),function(i)gIntersects(polys[comb[1,i]],polys[comb[2,i]])))){
				print("ERROR: OVERLAPPING USER DEFINED SUBDOMAINS!!!")
				stop()
			}
		}
	}else if (all(c("name","id")%in%names(domtable))){
		print("USING STATID TABLE")
		domtable = fread(namelist[["domainTable"]],header=T,keepLeadingZeros=T)
		if (length(unique(unique(domtable)$id)) != length(unique(domtable)$id)){
			print("ERROR: WRONGLY ATTRIBUTED STATIONS IN USER DEFINED STATION LIST!!!")
			stop()
		}
		if(!any(c("SYNOP","WMO")%in%domains)) statidList = domtable[["id"]]
	}else{
		print("ERROR: WRONG DOMAIN TABLE!!!")
		stop()
	}
}


# SET NUMBER OF CORES. IF NOT SET, ONLY ONE CORE WILL BE USED
mc.cores = 1
mc.cores = ifelse(!is.na(args[2]),as.numeric(args[2]),1)
print(paste0("USING ",mc.cores," CORE(S)"))


# LOAD FILES FOR EXPERIMENT OR DAILY ROUTINE
if(namelist[["experiment"]]%in%c("T","TRUE")){
	filelist = list() 
	for (i in 1:length(expID)){
	  filelist[[i]] = list.files(fdbkDir[i], full.names = F, pattern=namelist[["filePattern"]])
	}
	files = Reduce(intersect,filelist)
}else{
	files = c() 
	for (i in 1:length(expID)){
	  files = sort(unique(c(files,list.files(fdbkDir[i], full.names = F, pattern=namelist[["filePattern"]]))))
	}
}
if (length(c(namelist[["startValDate"]],namelist[["stopValDate"]]))==2){
		filesind   = which(str_sub(files,-10)%between%c(namelist[["startValDate"]],namelist[["stopValDate"]]))
		files      = files[filesind]
}

if (length(files)==0) stop("NO COMPARABLE FEEDBACK FILES WERE FOUND! CHECK FOR CORRECT PATH IN NAMELIST OR MATCHING FILENAMES")


# FUNCTION TO AGGREGATE SCORES
scorelist <- function(CRPS,CRPSF,ME,RMSE,SPREAD,OMEAN,FMEAN,LEN){
			sumLEN = sum(LEN,na.rm=T)
			ME     = sum((ME*LEN)/sumLEN,na.rm=T)
            OMEAN  = sum((OMEAN*LEN)/sumLEN,na.rm=T)
            FMEAN  = sum((FMEAN*LEN)/sumLEN,na.rm=T)
			SPREAD = sum((SPREAD*LEN)/sumLEN,na.rm=T)
			OMEAN  = sum((OMEAN*LEN)/sumLEN,na.rm=T)
            CRPS   = sum((CRPS*LEN)/sumLEN,na.rm=T)
            CRPSF  = sum((CRPSF*LEN)/sumLEN,na.rm=T)
			RMSE   = sqrt(sum(LEN*RMSE^2/sumLEN, na.rm = T))
			return(list(CRPS=CRPS,CRPSF=CRPSF,ME=ME,RMSE=RMSE,SPREAD=SPREAD,OMEAN=OMEAN,FMEAN=FMEAN,LEN=sumLEN))
}


# LOAD DATA FROM FEEDBACK FILES
parfun <- function(interm){

	SCORES = c()
	for (file in interm){
		DT = c()
		for (mod in 1:length(expID)){
			fnames   = paste0(fdbkDir[mod],"/",file)
			fnames   = fnames[file.exists(fnames)]
			cond     = list(obs                = "!is.na(obs)",
                            obstype            = "obstype==1",
                            codetype           = "codetype%in%c(11,14,20,140)",
                            veri_ens_member    = "veri_ens_member>=1",
                            time               = "time%between%range(timeBreaks)",
                            flags              = "!bitcheck(flags,c(10))")
			if (exists("statidList"))                    {cond$statid=paste0("strsplit(statid,split=' +')%in%",statidList)}
			if (exists("varnos"))                        {cond$varno="varno%in%varnos"}
			if (!is.null(namelist[["lonlims"]]))         {cond$lon=paste0("lon%between%c(",namelist[["lonlims"]],")")}
			if (!is.null(namelist[["latlims"]]))         {cond$lat=paste0("lat%between%c(",namelist[["latlims"]],")")}
			if (!is.null(namelist[["iniTimes"]]))        {cond$veri_initial_date = paste0("as.numeric(substr(veri_initial_date,9,10))%in%c(",namelist$iniTimes,")")}
			if (!is.null(namelist[["veri_ens_member"]])) {cond$veri_ens_member=paste0("veri_ens_member%in%c(",veri_ens_member[mod],")")}
			if (!is.null(namelist[["state"]]))           {cond$state = paste0("state%in%c(",namelist[["state"]],")")}
			if (!is.null(namelist[["r_state"]]))         {cond$r_state = paste0("r_state%in%c(",namelist[["r_state"]],")")}
			if (!is.null(namelist[["veri_run_class"]]))  {cond$veri_run_class = paste0("veri_run_class%in%c(",namelist[["veri_run_class"]],")")}
			if (!is.null(namelist[["veri_run_type"]]))   {cond$veri_run_type=paste0("veri_run_type%in%c(",namelist[["veri_run_type"]],")")}
            if (!is.null(namelist[["veri_forecast_time"]]))   {cond$veri_forecast_time=paste0("veri_forecast_time%in%c(",namelist[["veri_forecast_time"]],")")}
			if (mimicVersus){ cond[["state"]]=NULL; cond[["r_state"]]=NULL}	

			# ATTRIBUTES TO BE READ FROM FEEDBACK FILE
			vars = c("obs","veri_data","veri_ens_member","veri_forecast_time","time","veri_initial_date","statid","z_station","z_modsurf","lat","lon","varno","level","level_typ","state","veri_run_type","bcor")

			# READING DATA
            print(paste0("LOADING",fnames))
			DTT      = fdbk_dt_wide(read_fdbk_large(fnames,cond,vars))
            if (!is.null(DTT)){
			    DTT[,c("i_body","l_body","veri_run_type"):=NULL]
			    DTT[,veri_model:=expID[mod]]
			    DT = .rbind.data.table(DT,DTT)
			    rm(DTT)

			    if(!is.null(whitelists) & length(DT)>0){
				    for (whitelist in whitelists){
					    print(paste0("APPLYING WHITELIST ",whitelist))
					    white = fread(whitelist,colClasses='character')[,varno:=varno_to_name(varno,rev=T)]
					    for (v in unique(white$varno)) DT = DT[!(varno==v & !trimws(statid)%in%white[varno==v]$statid)]
				    }
			    }
			    if(!is.null(blacklists)){
				    for (blacklist in blacklists){
					    print(paste0("APPLYING BLACKLIST ",blacklist))
					    black = fread(blacklist,colClasses='character',fill=T)[varno!="",varno:=varno_to_name(varno,rev=T)]
					    for (v in unique(black$varno)){
                            if (v != "") DT = DT[!(varno==v & trimws(statid)%in%black[varno==v]$statid)]
                            if (v == "") DT = DT[!(trimws(statid)%in%black[varno==v]$statid)]
                        }
				    }
			    }
            }
		}


		# RENAME VARNO 29 (RH) TO VARNO 58 (RH2M)
		DT[varno==29,varno:=58]


		# REMOVE PS (110) WITH BAD QUALITY FLAG IN Z (1)
		if (all(DT[,all(c(1,110)%in%varno),by=veri_model]$V1)){
			print("REMOVING PS WHEN Z IS REJECTED")
			by = names(DT)[!names(DT)%in%c("obs","varno","level_typ","level","state",as.character(1:1000))]
			DT = .rbind.data.table( merge(DT[varno==1 ,by,with=F],DT[varno==110],by=by), DT[varno!=110])
			DT = DT[varno!=1]
		}


        # APPLY USER DOMAIN RESTRICTION        
        if ("USER"%in%domains){
            XX = unique(DT[,c("lon","lat","statid"),with=F])
		    if (all(c("name","lon","lat")%in%names(domtable))){
			    for(reg in unique(domtable$name)) XX[point.in.polygon(lon,lat,domtable[name==reg,]$lon,domtable[name==reg,]$lat)%in%c(1,3),synop_region:=reg]
		    }else if (all(c("name","id")%in%names(domtable))){
			    for(reg in unique(domtable$name)) XX[trimws(statid)%in%domtable[name==reg,]$id,synop_region:=reg]
		    }
		    DT = merge(DT,XX,by=c("lon","lat","statid"))[!is.na(synop_region)][,synop_region:=NULL]
	    }


		# REMOVE UNREASLISTIC PRECIPITATON
		print("REMOVING UNREALISTIC PRECIP")
		DT = DT[!(varno==80 & obs>100)]


	    # ROUND RR JUST LIKE OBSERVATIONS
		memCols = names(DT)[which(names(DT)%in%1:1000)]
		DT[varno==80, (memCols):=round(.SD,1),.SDcols=memCols]


		# REMOVE REJECTED OBSERVATIONS, EXCEPT FOR WIND
		# REMOVE WIND STATIONS WITH TOO LARGE DIFFERENCE IN Z_MODSURF AND LEVEL
		# THIS SHOULD ONLY APPLY TO COSMO!
		if (!mimicVersus){
			print("REMOVING UNRELIABLE WIND REPORTS")
			DT = DT[ (state!=7 & !varno%in%c(41,42,111,112)) | varno%in%c(41,42,111,112)]
			DT = DT[ !(varno%in%c(41,42,111,112) & abs(z_station-z_modsurf)>50)]
			DT = DT[ !(varno%in%c(41,42,111,112) & state%in%c(7) & z_station<=100) ]
		}
		DT[,z_modsurf:=NULL]	
		DT[,z_station:=NULL]	


		# RENAME VARNO AND ADD TIME RANGE FOR NON LOCAL VARIABLES
		print("RENAMING VARNO")
		DT[,varno:=varno_to_name(varno)]
		DT[level_typ==79,varno:=paste0(varno,"_",level,"h")]
		DT[,c("level","level_typ"):=NULL]


		# KEEP VARIABLES SPECIFIED IN NAMELIST
		print("REMOVE VARIABLES NOT GIVEN IN NAMELIST")
		DT       = DT[varno%in%unique(strsplit(namelist$varno,",")[[1]])]


		# CONVERT JOULE IN WATT FOR RADIATION
		print("CONVERT RADIATION")
		memColsObs = c("obs",memCols)
		DT[varno%in%c("RAD_DF_1h","RAD_GL_1h"),(memColsObs):=.SD/3600,.SDcols=memColsObs]
		DT[varno%in%c("RAD_DF_3h","RAD_GL_3h"),(memColsObs):=.SD/10800,.SDcols=memColsObs]
		DT[varno%in%c("RAD_DF_6h","RAD_GL_6h"),(memColsObs):=.SD/21600,.SDcols=memColsObs]		


		# REMOVE UNREALISTIC GLOBAL RADIATION
		DT = DT[!(startsWith(varno,"RAD_GL") & obs>2000)]
		DT = DT[!(startsWith(varno,"RAD_DF") & obs>500)]
		

        # REMOVE UNREALISTIC GUSTS
		DT = DT[!(startsWith(varno,"GUST") & obs>75)]


		# INTRODUCE BIAS CORRECTED VARIABLES
		if (useObsBias){
		print("REMOVING BIAS CORRECTION")
			biasedVars = DT[,any(bcor!=0),by=varno][V1==T]$varno
			DTT        = copy(DT[varno%in%biasedVars])
			DTT[,varno:=paste0(varno,"_raw")]
			DTT[,obs:=obs-bcor]
			DT = .rbind.data.table(DT,DTT,use.names=T)
			rm(DTT)
			namelist[['varno']] = paste(namelist[['varno']],paste0(paste0(unlist(strsplit(namelist[['varno']],",")),"_raw"),collapse=","),sep=",")
		}
		DT[,bcor:=NULL]		


        # REMOVE FORECASTS WITH TOO VIEW MEMBERS
		print("REMOVE FORECASTS WITH TOO VIEW MEMBERS")
		DT[,Nmems := length(memCols)-rowSums(is.na(.SD)),.SDcols=memCols]
		DT = DT[Nmems%in%nMembers]


		# APPLY TIME BINS
		print("FORECAST TIME BINNING")
		DT[!time%in%timeSteps,time:=as.numeric(as.character(cut(time,breaks=sort(timeBreaks),labels=sort(timeSteps))))]
		DT[,veri_forecast_time:=veri_forecast_time+(time/.6)]
		DT = DT[veri_forecast_time>=0]
		DT[,time:=NULL]


	    # THIS MIGHT HELP OBSERVATION ADJUSTMENT IN  CASES  LAT LON IS NOT AVAILABLE IN WITH THE SAME PRECICION FOR DIFFERENT MODELS
	    DT[, c("lon","lat"):=round(.SD,2),.SDcols=c("lon","lat")]


		# KEEP ONLY OBS AVAIL IN BOTH MODELS
		alignObs=TRUE
		if (!is.null(namelist[["alignObs"]])){
			if (namelist[["alignObs"]]%in%c("F","FALSE")){
				print("!!!NO OBSERVATION ALIGNMENT!!!!")
				alignObs=FALSE
			}
		}
		if (alignObs){
			print("ALIGNING OBSERVATIONS")
            notConsider = c("obs","Nmems",names(DT)[names(DT)%in%as.character(1:1000)]) # THESE VARIABLES DO NOT HAVE TO AGREE BETWEEN EXPERIMENTS
            DT = align(DT[!duplicated(DT[,-notConsider,with=F])],by="veri_model",notConsider)
            print(DT[,.N,by=veri_model])
			print("DATA ALIGNMENT DONE")
		}


		# FORMATTING TIME INFO
		print("TIME INFO REFORMATTING")
		DT[,ini_time:=format(as.POSIXct(veri_initial_date,format="%Y%m%d%H%M"),"%H")];print("INI TIME DONE")
		DT[,veri_forecast_time:=str_pad(veri_forecast_time/100,3,pad=0)];print("VERI FORECAST TIME DONE")


		# ADD PERIOD		
		DT[,veri_period:=paste0(format(range(as.POSIXct(veri_initial_date,format="%Y%m%d%H%M")+as.numeric(veri_forecast_time)*3600),"%Y%m%d%H%M"),collapse="-")]
        DT[,veri_initial_date:=NULL]

		# SINGLE RUN LEAD-TIME TO TIME OF DAY
		if (!is.null(namelist[["expType"]])){
			if (namelist[["expType"]]=="singlerun"){
				print("CONVERTING LEAD-TIME TO TIME OF DAY")
				DT[,veri_forecast_time:=(as.numeric(ini_time)+as.numeric(veri_forecast_time))%%24]
			}
		}

        # PRECALCULATION OF SCORES SATION BASED
        DT[,"FMEAN"  := rowMeans(.SD, na.rm = TRUE),.SDcols=memCols]
        DT[,"SPREAD" := rowSds(as.matrix(.SD), na.rm = TRUE),.SDcols=memCols]
        DT[,"CRPS"   := enscrps_cpp(as.matrix(.SD),obs,R_new=NA),.SDcols=memCols] 
        DT[,"CRPSF"  := enscrps_cpp(as.matrix(.SD),obs,R_new=Inf),.SDcols=memCols] # fair crps
        setnames(DT,"obs","OMEAN")
        DT[,"ME"     :=FMEAN-OMEAN]
        DT[,"RMSE"   :=abs(ME)]
        DT[,"LEN"    :=1]


        # REMOVE MEMBERS
        DT = DT[,-memCols,with=F]
        DT[,state:=NULL]
        DT[,Nmems:=NULL]


        # CALC SCORES AND ADD TO PREVIOUS SCORES
        SCORES = .rbind.data.table(SCORES,DT, use.names = TRUE)
        rm(DT)


        # AGGREGATE SCORE
        print("AGGREGATING SCORES FROM FILE")
        SCORES[,veri_period:=paste0(range(unlist(strsplit(veri_period,"-"))),collapse="-")]
        strat =  "statid,lat,lon,veri_forecast_time,veri_model,varno,ini_time,veri_period"
        SCORES = SCORES[,scorelist(CRPS,CRPSF,ME,RMSE,SPREAD,OMEAN,FMEAN,LEN),by=strat]
	}	

	SCORES[,veri_period:=paste0(range(unlist(strsplit(veri_period,"-"))),collapse="-")]
	return(SCORES)
}

files   = stri_reverse(sort(stri_reverse(files)))
groups  = lapply(splitIndices(length(files),min(mc.cores,max(1,floor(length(files)/2)))),function(i) files[i])
SCORES  = mclapply(groups,parfun,mc.cores=mc.cores,mc.preschedule=F)
SCORES  = SCORES[sapply(SCORES,is.data.table)]


print("FINAL AGGREGATION")
strat  =  "statid,lat,lon,veri_forecast_time,veri_model,varno,ini_time,veri_period"
SCORES = rbindlist(SCORES)[,veri_period:=paste0(range(unlist(strsplit(unique(veri_period),"-"))),collapse="-")][,scorelist(CRPS,CRPSF,ME,RMSE,SPREAD,OMEAN,FMEAN,LEN),by=strat]


# SORT
print("SORTING TABLE")
setkey(SCORES,ini_time,veri_forecast_time)


# RENAME STATID FOR APP
setnames(SCORES,"statid","ident")


# ADD SOME MORE SCORES
SCORES[,"SD":=sqrt(RMSE^2-ME^2)]
SCORES[,`SPEAD/RMSE`:=SPREAD/RMSE]
SCORES[,`SPEAD/SD`:=SPREAD/SD]


# SORT MODELS ACCORDING TO NAMELIST
SCORES[,veri_model:=factor(veri_model,levels=expID)]


# SAVE SCOREFILE FOR SHINY APPS
print("SAVING OUTPUT")
filename = paste0(namelist[["outDir"]],"/SYNOPe_bs_",namelist[["fileDescr"]],".Rdata")
scores = SCORES
save(scores,file=filename)
if (!is.null(shinyServer)){
	system(paste0("chmod uog+r ",filename))
	system(paste0("ssh ",shinyServer," mkdir -p ",shinyAppPath,"/fdbk_synop_eps_bystat/data/"))
	system(paste0("scp ",filename," ",shinyServer,":",shinyAppPath,"/fdbk_synop_eps_bystat/data/"))
	system(paste0("rm ",filename))
}

print(warnings())
print(system("qstat -f -J $PBS_JOBID ",intern=T))
print(paste0("R used ",sum(gc()[,6])/1e3," Gb"))














