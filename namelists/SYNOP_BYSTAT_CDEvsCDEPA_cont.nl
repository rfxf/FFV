expIds		"ILAM,ILAMP"

fileDescr	"ILAM_ALL_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "verSYNOP.YYYYMM"


experiment      "TRUE"

iniTimes	"0,12"

varnoContinuous           "T2M,RH2M,PS,TD2M,N_L,N_M,N_H,N,FF,DD,RAD_GL_1h,RAD_DF_1h,RR_1h,GUST_1h"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       "0,-60,-120,-180"

timeBreaks      "0,-30,-90,-150,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
