experiment	"T"

expIds		"ILAM,IEU"

expDescr	"ILAM-IEU"

fileDescr	"ILAM_IEU_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

filePattern     "verSYNOP.YYYYMM"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123


sigTest         "T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
