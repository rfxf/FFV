expIds		"ICON,IFS"

fileDescr	"ICON_IFS_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/"

outDir		"/hpc/rhome/routver/ffundel/temp/"

filePattern     "verSYNOP.YYYYMM"


experiment      "TRUE"

iniTimes	"0,12"

varnoContinuous           "T2M,RH2M,RH,PS,TD2M,N,FF,DD,RAD_GL_6h,RAD_DF_6h,RR_6h,GUST_6h,N_L,N_M,N_H,JJ_12h,TMIN_12h,TMAX_12h"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5,11'
