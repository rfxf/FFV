experiment  	"T"

expIds	    	"ILAMe,ILAMPe"

expDescr	    "ILAMe-ILAMPe"

fdbkDirs	    "/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir		    "/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "vepAIREP.YYYYMM"

fileDescr       "ILAM-EPS_YYYYMM_MNTHLY"


sigTest         "T"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"
