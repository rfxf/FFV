experiment	"T"

expIds		"ILAMe,IEUe"

expDescr	"ILAMe-IEUe"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

fileDescr       "ILAM_IEU_YYYYMM_MNTHLY"

filePattern     "vepTEMP"


startValDate    YYYYMM0100
stopValDate     YYYYMM3123

iniTimes    "0,12"

iniTimes  	"0,6,12,18"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
