experiment	'F'


expIds		'IEUe,IEUP1e'

filePattern	'vepSYNOP.YYYYMM'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/icon_nest/'

subdomains      "ALL,GER"

expDescr	'IEUE-IEUEP1'

fileDescr	'ICON-EUe_ALL_ROUTINES_YYYYMM_MNTHLY'

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"

sigTest         'T'
