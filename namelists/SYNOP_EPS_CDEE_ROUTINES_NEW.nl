experiment	'T'

varno		'Z,T2M,TD2M,U10M,V10M,RH2M,N_L,RR_1h,N,N_M,N_H,PS,DD,FF,RAD_GL_1h,RAD_DF_1h,GUST_1h'

thresholds      list('GUST_1h'=c(5,14),'RR_1h'=c(0.1,1,2))


expIds		'ILAMe,ILAMPe'

expDescr	'ILAMe-ILAMPe'

fdbkDirs	'/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/'

filePattern	'vepSYNOP'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/ilam/'

subdomains      "ALL,GER"

iniTimes	'0,12'

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

nMembers	"20"

timeSteps       "0,-60,-120,-180"

timeBreaks      "0,-30,-90,-150,-180"

veri_run_type   '0,3'

veri_run_class  '0,2'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
