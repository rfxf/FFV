varnoContinuous	"T2M,RH2M,PS,TD2M,FF,DD,TMIN_12h,TMAX_12h,JJ_12h,N,N_L,N_H,N_M,RAD_GL_3h,RAD_DF_3h,RR_3h,GUST_3h"

pecthresholds	list('N'=list('lower'=c(-1),'upper'=c(1)),'T2M'=list('lower'=c(-2),'upper'=c(2)),'TD2M'=list('lower'=c(-2),'upper'=c(2)),'TMIN_12h'=list('lower'=c(-2),'upper'=c(2)),'JJ_12h'=list('lower'=c(-2),'upper'=c(2)),'PS'=list('lower'=c(-500),'upper'=c(500)),'FF'=list('lower'=c(-3),'upper'=c(3)),'DD'=list('lower'=c(-30),'upper'=c(30)))

catthresholds	list('GUST_1h'=c(5,12,15,20,25),'RR_1h'=c(0.1,2,5),'N'=c(2,6))

experiment	"T"

expIds		"ILAM,IEU"

expDescr	"ILAM-IEU"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

subdomains	"ALL,CDE,GER"

filePattern     "verSYNOP"

iniTimes	"0,12"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       "0"

timeBreaks      "0,-30"

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5,11'

r_state         '0,1,5'
