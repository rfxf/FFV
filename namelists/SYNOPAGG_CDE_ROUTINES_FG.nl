experiment	"F"

expIds		"ILAM,ILAMP,ILAMe,ILAMPe"

expDescr	"ILAM-ILAMP"

fileDescr	"ILAM_FG_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_fg/"

subdomains	"ALL,GER"

filePattern     "verSYNOP."



startValDate	YYYYMM0100
stopValDate	YYYYMM3123

#iniTimes        "0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
