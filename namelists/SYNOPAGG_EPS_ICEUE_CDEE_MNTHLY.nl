experiment	"T"


expIds		"ILAMe,IEUe"

expDescr	"ILAMe-IEUe"

filePattern     "vepSYNOP.YYYYMM"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

subdomains	"ALL,CDE,GER"

fileDescr	'IEUE-ILAME_ROUTINES_YYYYMM_MNTHLY'

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"

sigTest         'T'
