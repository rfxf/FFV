varnoContinuous	"T2M,RH2M,PS,TD2M,FF,DD,RAD_GL_1h,RAD_DF_1h,TMIN_12h,TMAX_12h,JJ_12h,N,N_L,N_M,N_H,RR_1h,GUST_1h"

pecthresholds	list('N_L'=list('lower'=c(-1),'upper'=c(1)),'N_M'=list('lower'=c(-1),'upper'=c(1)),'N_H'=list('lower'=c(-1),'upper'=c(1)),'N'=list('lower'=c(-1),'upper'=c(1)),'T2M'=list('lower'=c(-2),'upper'=c(2)),'TD2M'=list('lower'=c(-2),'upper'=c(2)),'TMIN_12h'=list('lower'=c(-2),'upper'=c(2)),'JJ_12h'=list('lower'=c(-2),'upper'=c(2)),'PS'=list('lower'=c(-500),'upper'=c(500)),'FF'=list('lower'=c(-3),'upper'=c(3)),'DD'=list('lower'=c(-30),'upper'=c(30)))

catthresholds	list('GUST_1h'=c(5,12,15,20,25),'RR_1h'=c(0.1,2,5),'N_L'=c(2,6),'N_M'=c(2,6),'N_H'=c(2,6),'N'=c(2,6))

experiment	"T"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

subdomains	"ALL,GER"

filePattern     "verSYNOP"


whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       "0,-60,-120,-180"

timeBreaks      "0,-30,-90,-150,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
