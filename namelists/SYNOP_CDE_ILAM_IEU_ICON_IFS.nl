varnoContinuous 'T2M,RH,RH2M,PS,TD2M,FF,DD,RAD_GL_6h,RAD_DF_6h,TMIN_12h,TMAX_12h,JJ_12h,N,N_L,N_M,N_H'
catthresholds   list('GUST_6h'=c(12,15,20,25),'RR_6h'=c(0.1,1,2))
subdomains      'ALL,GER'
filePattern     'verSYNOP.'
sigTest         'T'
expType         'standard'
experiment      'T'
expIds          'ILAM,IEU,ICON,IFS'
expDescr        'ILAM,IEU,ICON,IFSffundel'
fdbkDirs        '/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/'
outDir          '/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu_icon_ifs/'
whitelists      '/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds'
blacklists	    "/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"
shinyServer     'oflxs464'
shinyAppPath    '/uwork1/ffundel/shiny'
lonlims         '-35,64'
latlims         '26,71'

timeSteps       "0"

timeBreaks      "0,-30"

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
