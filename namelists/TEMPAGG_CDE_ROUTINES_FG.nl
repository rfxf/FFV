experiment	"T"

expIds		"ILAM,ILAMP,ILAMe,ILAMPe"

expDescr	"ILAM-ILAMP"

fileDescr	"ILAM_FG_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_fg/"

filePattern     "verTEMP."

sigTest		"T"

startValDate  YYYYMM0100
stopValDate	  YYYYMM3123

shinyServer	  "oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
