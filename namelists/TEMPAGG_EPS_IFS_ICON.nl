experiment	"T"

expIds		"ICONe,ICONP1e,IFSe"

expDescr	"ifs_icon"

fileDescr	"IFS_ICON_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/"

filePattern     "vepTEMP"


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

iniTimes    "0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
