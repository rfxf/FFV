varnoContinuous		"U,V,FF,DD"

experiment	"T"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "verPILOT"


sigTest		"F"

alignObs	"TRUE"

iniTimes	"0,12"

subdomains	"ALL,GER"

timeSteps       "0,-60,-120,-180"

timeBreaks      "0,-30,-90,-150,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'
