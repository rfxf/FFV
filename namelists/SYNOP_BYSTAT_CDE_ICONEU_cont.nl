expIds		"ILAM,IEU"

fileDescr	"ILAM_IEU_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "verSYNOP.YYYYMM"

experiment      "TRUE"

varnoContinuous           "T2M,RH,RH2M,PS,TD2M,N_L,N_M,N_H,N,FF,DD,RAD_GL_3h,RAD_DF_3h,RR_3h,GUST_3h"


iniTimes	"0,12"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       "0"

timeBreaks      "0,-30"

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
