experiment	"F"

expIds		"ICON,ICONP1,ICONP"

expDescr	"ALL_GLOBAL_ROUTINES"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

filePattern     "verSCATT."


timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'
