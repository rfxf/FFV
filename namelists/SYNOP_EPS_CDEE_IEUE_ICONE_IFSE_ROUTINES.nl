experiment	'T'

varno		'Z,T2M,TD2M,U10M,V10M,RH,RH2M,N_L,RR_6h,N,N_M,N_H,PS,DD,FF,RAD_GL_6h,RAD_DF_6h,GUST_6h'

thresholds      list('GUST_6h'=c(5,12,14),'RR_6h'=c(0.1,1,2))

expIds		'ILAMe,IEUe,ICONe,IFSe'

expDescr	'ILAMe-IEUe-ICONe-IFSe'

fdbkDirs	'/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/'

filePattern	'vepSYNOP'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu_icon_ifs/'

subdomains      "ALL,GER"

iniTimes	'0,12'

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

lonlims         '-35,64'

latlims         '26,71'

nMembers	"20,20,40,40,50"

timeSteps       "0"

timeBreaks      "0,-30"

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
