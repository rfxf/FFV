expIds		"ICON,ICONP,ICONP1"

fileDescr	"ICON_GLOBAL_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/"

outDir          "/hpc/rhome/routver/ffundel/temp/"

filePattern     "verTEMP.YYYYMM"


experiment      "TRUE"

varnoContinuous "Z,T,RH,TD,FF,DD"

alignObs	"T"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

alignObs        'REDUCED'
