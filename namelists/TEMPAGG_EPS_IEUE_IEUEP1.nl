experiment	"F"

expIds		"IEUe,IEUP1e"

expDescr	"IEUE-IEUEP1"

fileDescr       "ICON_NEST_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"


filePattern     "vepTEMP"

startValDate    YYYYMM0100
stopValDate     YYYYMM3123

iniTimes	"0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
