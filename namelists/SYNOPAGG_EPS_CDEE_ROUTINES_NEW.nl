experiment	    'T'

expIds          'ILAMe,ILAMPe'

expDescr	    'ILAMe-ILAMPe'

filePattern     'vepSYNOP.YYYYMM'

outDir		    '/hpc/rhome/routver/ffundel/FFV/output/ilam/'

subdomains      'ALL,GER'

fileDescr       'ILAMe_ALL_ROUTINES_YYYYMM_MNTHLY'

shinyServer     'oflxs464'

sigTest         'T'

shinyAppPath	'/uwork1/routver/shiny/'
