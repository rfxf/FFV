varnoContinuous	"T2M,RH2M,RH,PS,TD2M,FF,DD,RAD_GL_3h,RAD_DF_3h,TMIN_12h,TMAX_12h,JJ_12h,N,N_L,N_M,N_H,RR_3h,GUST_3h"

pecthresholds	list('N_L'=list('lower'=c(-1),'upper'=c(1)),'N_M'=list('lower'=c(-1),'upper'=c(1)),'N_H'=list('lower'=c(-1),'upper'=c(1)),'N'=list('lower'=c(-1),'upper'=c(1)),'T2M'=list('lower'=c(-2),'upper'=c(2)),'TD2M'=list('lower'=c(-2),'upper'=c(2)),'TMIN_12h'=list('lower'=c(-2),'upper'=c(2)),'JJ_12h'=list('lower'=c(-2),'upper'=c(2)),'PS'=list('lower'=c(-500),'upper'=c(500)),'FF'=list('lower'=c(-3),'upper'=c(3)),'DD'=list('lower'=c(-30),'upper'=c(30)))

catthresholds	list('GUST_3h'=c(5,12,15,20,25),'RR_3h'=c(0.1,2,5),'N_L'=c(2,6),'N_M'=c(2,6),'N_H'=c(2,6),'N'=c(2,6))

experiment	"T"

expIds		"IEU,IEUP,IEUP1"

expDescr	"ALL_ICON_NEST_ROUTINES"

fileDescr	"ICON_NEST_ALL_ROUTINES_YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup1/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"

subdomains	"ALL,CEU,CDE,GER"

filePattern     "verSYNOP.2022"


startValDate	YYYYMM0100
stopValDate	YYYYMM3123


whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5'
