expIds		"IEU,IEUP,IEUP1"

fileDescr	"ICON_EUNEST_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup1/"

outDir		"/hpc/rhome/routver/ffundel/temp/"

filePattern     "verSYNOP.YYYYMM"


experiment      "TRUE"

lonlims		"-35,64"
latlims		"26,71"

iniTimes	"0,12"

varnoContinuous           "T2M,RH2M,RH,PS,TD2M,N_L,N_M,N_H,N,FF,DD,RAD_GL_3h,RAD_DF_3h,RR_3h,GUST_3h"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"
timeSteps       "0"
timeBreaks      "30,-30"
