experiment	"LASTPARAORP1"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fileDescr   "ILAM_ROUTINES_SINCE"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

subdomains	"ALL"

filePattern "verTEMP."

sigTest		"T"

iniTimes        "0,12"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
