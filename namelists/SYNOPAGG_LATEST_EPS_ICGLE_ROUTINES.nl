experiment	'LASTPARAORP1'


expIds		'ICONe,ICONP1e'

filePattern	'vepSYNOP'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/'

subdomains	'ALL,LAT,SYNOP,CEU,CDE,GER'

expDescr        'ICONe_ICONP1e_IFSe'

fileDescr	'ICONe_GLOBAL_ROUTINES_SINCE'

iniTimes        "0,12"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"

sigTest     'T'
