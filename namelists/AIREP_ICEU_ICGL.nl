varnoContinuous           "T,RH,FF,DD"

experiment	"T"

expIds		"IEU,IEUP,IEUP1"

expDescr	"ICEU_ALL_ROUTINES"

fileDescr	"ICEU_ALL_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup1/"

acftDir		"/hpc/rhome/routver/ffundel/FFV/aircrafts/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"

subdomains	"ALL,CDE,GER"

filePattern     "verAIREP.2022"


# Aircraft types
acfttypes       "LH_mrF,EU0822,EU0831,EU0840,EU0841,EU0882,EU0883,EU0884,EU0886,EU0897,A300,A319,A320,A321,A340,A330,B737,B747"

phase           "2,3,4,5,6,7"

phase_upp_plev  "300,300,300,0,0,300"

codetype        "141,144,145,146"

state           "0,1,5"

bin_levels	"1000,975,950,925,900,875,850,825,800,750,700,650,600,550,500,450,400,350,300,250"

iniTimes        '0,12'

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'
