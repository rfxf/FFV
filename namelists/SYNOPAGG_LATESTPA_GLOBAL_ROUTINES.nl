experiment      "LASTPARAORP1"

expIds          "ICON,ICONP,ICONP1"

expDescr        "ALL_GLOBAL_ROUTINES"

fileDescr       "ICON_GLOBAL_ROUTINES_SINCE"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

subdomains      "LAT,WMO,CDE,CEU"

filePattern     "verSYNOP."

iniTimes        "0,12"

sigTest         "T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
