varnoContinuous	"T,RH,TD,FF,DD,QV,QV_N"

experiment	"T"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fileDescr	"ILAM-YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

subdomains	"ALL"

filePattern     "verTEMP."


customLevels	"1000,950,900,850,800,750,700,650,600,550,500,450,400,350,300,250,200,150,100"

timeSteps       "0"

timeBreaks      "0,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

alignObs        'REDUCED'
