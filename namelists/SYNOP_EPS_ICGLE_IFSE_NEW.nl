experiment	'F'

varno		'T2M,TD2M,U10M,V10M,RH,RH2M,N_L,RR_6h,N,N_M,N_H,PS,DD,FF,RAD_GL_6h,RAD_DF_6h,GUST_6h'

thresholds      list('GUST_6h'=c(5,14,18),'RR_6h'=c(0.1,1,2))


expIds		'ICONe,ICONP1e,IFSe'

fdbkDirs	'/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/'

filePattern	'vepSYNOP'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/'

expDescr	'ICONe_ICONP1e_IFSe'

subdomains	'ALL,LAT,SYNOP,CEU,CDE,GER'

iniTimes	'0,12'

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

nMembers	"40,40,50"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5,11'
