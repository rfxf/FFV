experiment	"LASTPARAORP1"

expIds		"IEU,IEUP,IEUP1"

expDescr	"ALL_ICON_NEST_ROUTINES"

fileDescr	"ICON_NEST_ROUTINES_SINCE"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"

filePattern     "verSYNOP."


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

sigTest         "T"

iniTimes        "0,12"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
