experiment	"F"

expIds		"ICON,ICONP,ICONP1,ICONe,ICONP1e"

expDescr	"ALL_GLOBAL_ROUTINES"

fileDescr	"ICON_GLOBAL_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

filePattern     "verTEMP."

iniTimes     "0,12"

sigTest		"T"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
