expIds		"ILAM,ILAMP"

fileDescr	"ILAM_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/ilam"

filePattern     "verTEMP.YYYYMM"

experiment      "TRUE"

iniTimes	"0,12"

varnoContinuous "Z,T,RH,TD,FF,DD"

customLevels	"1000,950,900,850,800,750,700,650,600,550,500,450,400,350,300,250,200,150,100"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

timeSteps       "0"

timeBreaks      "0,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

alignObs        'REDUCED'
