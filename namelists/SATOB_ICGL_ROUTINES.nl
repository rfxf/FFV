varno		"111,112"

experiment	"F"

expIds		"ICON,ICONP,ICONP1"

expDescr	"ALL_GLOBAL_ROUTINES"

fileDescr	"ICON_GLOBAL_ALL_ROUTINES_YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

subdomains	"ALL,LAT"

filePattern     "verSATOB."


inclEnsMean	"TRUE"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'
