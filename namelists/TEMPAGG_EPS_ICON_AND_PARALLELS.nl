experiment	"F"

expIds		"ICONe,ICONP1e"

expDescr	"ALL_GLOBAL_ROUTINES"

fileDescr	"ICON_GLOBAL_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

filePattern     "vepTEMP"


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

iniTimes    "0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
