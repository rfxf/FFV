catthresholds	list('GUST_6h'=c(5,12,15,20,25),'RR_6h'=c(0.1,2,10),'N'=c(2,6))

expIds		"IEU,ICON"

fileDescr	"ICON_EUNEST_ALL_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/"

outDir		"/hpc/rhome/routver/ffundel/temp/"


filePattern     "verSYNOP.YYYYMM"

lonlims		"-35,64"
latlims		"26,71"

iniTimes	"0,12"

experiment      "TRUE"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5,11'
