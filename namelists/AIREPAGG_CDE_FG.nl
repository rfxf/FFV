experiment	"T"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fileDescr	"ILAM_FG_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_fg/"

filePattern     "verAIREP.YYYYMM"


startValDate	YYYYMM0100

stopValDate	YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
