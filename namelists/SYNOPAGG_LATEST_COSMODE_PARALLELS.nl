experiment	"LASTPARAORP1"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fileDescr    "ILAM_ROUTINES_SINCE"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

subdomains	"ALL"

filePattern     "verSYNOP."


iniTimes        "0,12"

sigTest         "T"

rerunLatest	"F"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
