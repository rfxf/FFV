catthresholds	list('GUST_1h'=c(5,12,15,20,25),'RR_1h'=c(0.1,2,5),'N_L'=c(2,6),'N_M'=c(2,6),'N_H'=c(2,6),'N'=c(2,6))

expIds		"ILAM,ILAMP"

fileDescr	"ILAM_ALL_ROUTINES_YYYYMM_MNTHLY"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ilamp/"

outDir          "/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "verSYNOP.YYYYMM"


experiment      "TRUE"

iniTimes	"0,12"

shinyServer     "oflxs464"

shinyAppPath    "/uwork1/routver/shiny/"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       "0,-60,-120,-180"

timeBreaks      "0,-30,-90,-150,-180"

veri_run_type   '0,2'

veri_run_class  '0,2'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
