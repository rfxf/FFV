varnoContinuous		"Z,T,RH,FF,QV,QV_N"

experiment	"T"

expIds		"ICONe,ICONP1e,IFSe"

expDescr	"ifs_icon"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/"

subdomains	"ALL,LAT,WMO,CDE,CEU"

filePattern     "vepTEMP"


alignObs	"T"

timeSteps       '0'

timeBreaks      '90,-90'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

nMembers        '40,50'

alignObs        'REDUCED'
