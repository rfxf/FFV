experiment	'LASTPARAORP1'


expIds		'IEUe,IEUP1e'

filePattern	'vepSYNOP'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/icon_nest/'

subdomains      "ALL,GER"

expDescr	'IEUE-IEUEP1'

fileDescr	'ICON-EUe_ALL_ROUTINES_SINCE'

iniTimes        "0,12"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"

sigTest     'T'
