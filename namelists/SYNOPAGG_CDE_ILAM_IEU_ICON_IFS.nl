experiment	"T"

expIds          'ILAM,IEU,ICON,IFS'

expDescr        'ILAM-IEU-ICON-IFS'

fileDescr	"ILAM_IEU_ICON_IFS_YYYYMM_MNTHLY"

outDir          '/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu_icon_ifs/'

filePattern     'verSYNOP.YYYYMM'


sigTest         "T"

startValDate	YYYYMM0100
stopValDate	    YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
