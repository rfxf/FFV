varnoContinuous	"Z,T,RH,TD,FF,DD,QV,QV_N"

experiment	"T"

expIds		"ILAM,IEU"

expDescr	"ILAM-IEU"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

subdomains	"ALL"

filePattern     "verTEMP.2022"


timeSteps       "0"

timeBreaks      '90,-90'

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5'

r_state         '0,1,5'

alignObs        'REDUCED'
