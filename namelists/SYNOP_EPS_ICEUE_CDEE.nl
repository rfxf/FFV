varno		"T2M,RH,RH2M,PS,TD2M,FF,TMIN_12h,TMAX_12h,JJ_12h,N,RR_3h,GUST_3h,N_L,N_M,N_H,RAD_GL_3h,RAD_DF_3h"

thresholds      list('GUST_3h'=c(5,14),'RR_3h'=c(0.1,1,2))

experiment	"T"

expIds		"ILAMe,IEUe"

expDescr	"ILAMe-IEUe"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

subdomains	"ALL,CDE,GER"

filePattern     "vepSYNOP"


iniTimes	"0,12"


whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

nMembers	"40,20"

timeSteps       "0"

timeBreaks      "0,-30"

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5,7,11'

r_state         '0,1,5,7,11'
