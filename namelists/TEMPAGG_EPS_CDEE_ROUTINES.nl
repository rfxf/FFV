experiment	"T"

expIds		"ILAMe,ILAMPe"

expDescr	"ILAMe-ILAMPe"

fileDescr	"ILAM-EPS_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

filePattern     "vepTEMP"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123

iniTimes    "0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
