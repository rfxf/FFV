varno		"162"

experiment	"T"

expIds		"ICON,IFS"

expDescr	"icon,ifs_icon"

fileDescr	"IFS_ICON_YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/"

subdomains	"ALL,LAT"

filePattern     "verGPSRO."


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'
