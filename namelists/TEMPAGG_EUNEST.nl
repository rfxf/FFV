experiment	"F"

expIds		"IEU,IEUP,IEUP1,ICON,ICONP,ICONP1"

expDescr	"ALL_EU_ROUTINES"

fileDescr	"ICON_EUNEST_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/eu_nest/"

filePattern     "verTEMP."


sigTest		"T"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
