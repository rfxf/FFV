varnoContinuous		"Z,T,TD,RH,FF,DD,QV,QV_N"

experiment	"F"

expIds		"ICON,ICONP1,ICONP"

expDescr	"ALL_GLOBAL_ROUTINES"

fileDescr	"ICON_GLOBAL_ALL_ROUTINES_YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

subdomains	"LAT,WMO,CDE,CEU"

filePattern     "verTEMP."


inclEnsMean	"TRUE"

startValDate	YYYYMM0100
stopValDate	YYYYMM3123

alignObs	"T"

timeSteps       '0'

timeBreaks      '90,-90'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

alignObs        'REDUCED'
