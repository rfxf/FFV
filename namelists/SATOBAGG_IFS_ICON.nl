varno		"111,112"

experiment	"T"

expIds		"ICON,ICONP1,IFS"

expDescr	"icon,ifs_icon"

fileDescr	"IFS_ICON_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/"

subdomains	"ALL,LAT"

filePattern     "verSATOB."


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
