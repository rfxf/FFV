experiment	"T"

expIds		"ILAM,ILAMP"

expDescr	"ILAM-ILAMP"

fileDescr	"ILAM_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam/"

subdomains	"ALL,GER"

filePattern     "verSYNOP."



startValDate	YYYYMM0100
stopValDate	YYYYMM3123

iniTimes        "0,12"

sigTest		"T"

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
