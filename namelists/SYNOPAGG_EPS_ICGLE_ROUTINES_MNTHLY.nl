experiment	'F'


expIds		'ICONe,ICONP1e'

fileDescr	'ICONe_ALL_ROUTINES_YYYYMM_MNTHLY'

filePattern	'vepSYNOP.YYYYMM'

outDir		'/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/'

expDescr        'ICONe_ICONP1e_IFSe'

subdomains	'ALL,LAT,SYNOP,CEU,CDE,GER'

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"

sigTest         'T'
