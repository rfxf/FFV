experiment	"T"

expIds		"ILAM,IEU"

expDescr	"ILAM-IEU"

fileDescr	"ILAM_IEU_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

filePattern     "verTEMP.YYYYMM"

sigTest		"T"

startValDate	YYYYMM0100
stopValDate	 YYYYMM3123


shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
