varnoContinuous	"T2M,RH2M,RH,PS,TD2M,FF,DD,RAD_GL_6h,RAD_DF_6h,TMIN_12h,TMAX_12h,JJ_12h,N,N_L,N_M,N_H"

pecthresholds	list('N_L'=list('lower'=c(-1),'upper'=c(1)),'N_M'=list('lower'=c(-1),'upper'=c(1)),'N_H'=list('lower'=c(-1),'upper'=c(1)),'N'=list('lower'=c(-1),'upper'=c(1)),'T2M'=list('lower'=c(-2),'upper'=c(2)),'TD2M'=list('lower'=c(-2),'upper'=c(2)),'TMIN_12h'=list('lower'=c(-2),'upper'=c(2)),'JJ_12h'=list('lower'=c(-2),'upper'=c(2)),'PS'=list('lower'=c(-500),'upper'=c(500)),'FF'=list('lower'=c(-3),'upper'=c(3)),'DD'=list('lower'=c(-30),'upper'=c(30)))

catthresholds	list('GUST_6h'=c(5,12,15,20,25),'RR_6h'=c(0.1,2,10),'N_L'=c(2,6),'N_M'=c(2,6),'N_H'=c(2,6),'N'=c(2,6))

experiment	"T"

expIds		"ICON,ICONP1,IFS"

expDescr	"icon,ifs_icon"

fileDescr	"IFS_ICON_YYYYMM"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/icon/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconp1/,/hpc/rwork2/routver/ffundel/DATA/FDBK/ifs/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ifs_icon/"

subdomains	"ALL,LAT,SYNOP,CDE,CEU"

filePattern     "verSYNOP."


#startIniDate	201507270000
#stopIniDate	20150631120000

startValDate	YYYYMM0100
stopValDate	YYYYMM3123


whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5'
