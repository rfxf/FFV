varno		"T2M,RH,RH2M,PS,TD2M,FF,TMIN_12h,TMAX_12h,JJ_12h,N,RR_3h,GUST_3h,N_L,N_M,N_H"

thresholds      list('GUST_3h'=c(5,14),'RR_3h'=c(0.1,1,2))

experiment	"F"

expIds		"IEUe,IEUP1e"

expDescr	"IEUE-IEUEP1"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup1/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"

subdomains	"ALL,GER"

filePattern     "vepSYNOP"


iniTimes	"0,12"

lonlims         "-35,64"
latlims         "26,71"

whitelists	"/hpc/rhome/routver/ffundel/FFV/whitelist/whitelist_clouds"
blacklists	"/hpc/rhome/routver/ffundel/FFV/blacklist/blacklist"

nMembers	"40"

timeSteps       '0'

timeBreaks      '30,-30'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5,11'

r_state         '0,1,5,11'
