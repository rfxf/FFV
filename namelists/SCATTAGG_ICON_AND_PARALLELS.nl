experiment	"F"

expIds		"ICON,ICONP1,ICONP"

expDescr	"ALL_GLOBAL_ROUTINES"

fileDescr	"ICON_GLOBAL_ALL_ROUTINES_YYYYMM_MNTHLY"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_global/"

subdomains	"ALL,LAT"

filePattern     "verSCATT."


startValDate	YYYYMM0100
stopValDate	YYYYMM3123

shinyServer	"oflxs464"

shinyAppPath	"/uwork1/routver/shiny/"
