varnoContinuous		"Z,T,RH,U,V,QV,QV_N"

experiment	"T"

expIds		"ILAMe,IEUe"

expDescr	"ILAMe-IEUe"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/ilam/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/ilam_ieu/"

subdomains	"CDE,GER"

filePattern     "vepTEMP.2022"


latlims            "44,57"
lonlims            "0,20"

timeSteps       "0"

timeBreaks      '90,-90'

veri_run_type   '0'

veri_run_class  '0'

state           '0,1,5'

r_state         '0,1,5'

nMembers        '20,40'

alignObs        'REDUCED'
