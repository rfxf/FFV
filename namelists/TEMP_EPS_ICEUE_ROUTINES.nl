varnoContinuous		"Z,T,RH,FF,QV,QV_N"

experiment	"T"

expIds		"IEUe,IEUP1e"

expDescr	"IEUE-IEUEP1"

fdbkDirs	"/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeu/,/hpc/rwork2/routver/ffundel/DATA/FDBK/iconeup1/"

outDir		"/hpc/rhome/routver/ffundel/FFV/output/icon_nest/"

subdomains	"ALL,CDE,GER"

filePattern     "vepTEMP.2022"


lonlims         "-35,64"
latlims         "26,71"

alignObs	"T"

timeSteps       '0'

timeBreaks      '90,-90'

veri_run_type   '0,4'

veri_run_class  '0,2'

state           '0,1,5'

r_state         '0,1,5'

nMembers        '40'

alignObs        'REDUCED'
